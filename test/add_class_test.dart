import 'package:dsc_project/app/home/list_view/validator.dart';
import 'package:test/test.dart';

void main() {
  test('empty class section returns validation message', () {
    var output = FieldValidator.validateClassSection('');
    expect(output, 'Class Section must not be blank');
  });

  test('empty class name returns validation message', () {
    var output = FieldValidator.validateClassName('');
    expect(output, 'Class Name must not be blank');
  });

  test('not empty class section returns null', () {
    var output = FieldValidator.validateClassSection('class section');
    expect(output, null);
  });

  test('not empty class name returns null', () {
    var output = FieldValidator.validateClassName('class name');
    expect(output, null);
  });
}
