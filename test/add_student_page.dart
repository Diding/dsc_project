import 'package:dsc_project/app/home/list_view/add_student_page.dart';
import 'package:dsc_project/app/home/list_view/add_student_view_model.dart';
import 'package:dsc_project/common_widgets/form_submit_button.dart';
import 'package:dsc_project/common_widgets/pop_up_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('can add student on add student page',
      (WidgetTester tester) async {
    //build widget
    await tester.pumpWidget(AddStudentPage(model: AddStudentPageViewModel()));

    //'enter values to textfield'
    await tester.enterText(find.byKey(ValueKey<int>(1)), 'Sachiko');
    await tester.enterText(find.byKey(ValueKey<int>(2)), 'Gubat');
    await tester.enterText(find.byKey(ValueKey<int>(3)), '09776542764');

    await tester.press(find.byType(FormSubmitButton));

    await tester.pumpAndSettle();

    expect(find.byType(PopUpMessage), findsNothing);
  });
}
