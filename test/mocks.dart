import 'package:mockito/mockito.dart';
import 'package:dsc_project/services/firebase_auth_service.dart';
import 'package:dsc_project/services/firestore_database.dart';

class MockAuthService extends Mock implements FirebaseAuthService {}

class MockDatabase extends Mock implements FirestoreDatabase {}