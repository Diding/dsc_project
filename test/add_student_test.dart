import 'package:dsc_project/app/home/list_view/validator.dart';
import 'package:test/test.dart';

void main() {
  test('null first name field', () {
    var output = FieldValidator.validateFirstName('');
    expect(output, 'Enter First Name');
  });
  test('null last name field', () {
    var output = FieldValidator.validateLastName('');
    expect(output, 'Enter Last Name');
  });
  test('null phone number field', () {
    var output = FieldValidator.validPhoneNumber('');
    expect(output, 'Enter Contact Number');
  });
  test('first name entered was valid', () {
    var output = FieldValidator.validateFirstName('Sachiko');
    expect(output, null);
  });
  test('last name entered was valid', () {
    var output = FieldValidator.validateLastName('Gubat');
    expect(output, null);
  });
  test('phone number entered was valid sample 1', () {
    var output = FieldValidator.validPhoneNumber('09154532261');
    expect(output, null);
  });
  test('phone number entered was valid sample 2', () {
    var output = FieldValidator.validPhoneNumber('+639154532261');
    expect(output, null);
  });
  test('phone number entered was invalid', () {
    var output = FieldValidator.validPhoneNumber('+63119154532261');
    expect(output, 'Enter Valid Number');
  });
}
