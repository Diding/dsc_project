# checkEd

In the Philippines, cutting classes is a rampant problem especially in public schools where there is inefficient security and huge number of students. Most of these students’ parents are unaware of their child’s status in school mainly in terms of attendance. As solution, we developed checkEd, a parent monitoring app that allows the parent to monitor the child’s attendance in every subject in real-time via text message. Since Philippines is known as the “text capital of the world” and most elderly people and whose child goes to a public school in a third world country can only afford a basic phone which can only be used in text messaging and phone calls, we used texting as the best option to notify the parents. Because of the large volume of students in each class, the teacher cannot keep track on each child’s performance. The application also provides a summary of each student’s attendance which can solve the teacher’s problem in monitoring each one of them.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- Dart
- Flutter
- Android Studio

### Installing

Install an emulator. Then run the following commands:

- flutter doctor(checking the necessary requirements to run a flutter application)
- flutter pub get
- flutter run
