class Strings {
  // Generic strings
  static const String ok = 'OK';
  static const String cancel = 'Cancel';

  // Logout
  static const String logout = 'Logout';
  static const String logoutAreYouSure =
      'Are you sure that you want to logout?';
  static const String logoutFailed = 'Logout failed';

  // Sign In Page
  static const String signIn = 'Sign in';
  static const String signInWithEmailPassword =
      'Sign in with email and password';

  // Email & Password page
  static const String register = 'Register';
  static const String forgotPassword = 'Forgot password';
  static const String forgotPasswordQuestion = 'Forgot password?';
  static const String createAnAccount = 'Create an account';
  static const String needAnAccount = 'Need an account? Register';
  static const String haveAnAccount = 'Have an account? Sign in';
  static const String signInFailed = 'Sign in failed';
  static const String registrationFailed = 'Registration failed';
  static const String passwordResetFailed = 'Password reset failed';
  static const String sendResetLink = 'Send Reset Link';
  static const String backToSignIn = 'Back to sign in';
  static const String resetLinkSentTitle = 'Reset link sent';
  static const String resetLinkSentMessage =
      'Check your email to reset your password';
  static const String emailLabel = 'Email Address';
  static const String emailHint = 'Email Address';
  static const String password8CharactersLabel = 'Password (8+ characters)';
  static const String passwordLabel = 'Password';
  static const String invalidEmailErrorText = 'Email is Invalid';
  static const String invalidEmailEmpty = 'Email can\'t be empty';
  static const String invalidPasswordTooShort = 'Password is too short';
  static const String invalidPasswordEmpty = 'Password can\'t be empty';

  // Home page
  static const String homePage = 'Home Page';
  static const String dashboard = 'Dashboard';
  static const String emailAddress = 'Email Address: ';

  // Account page
  static const String account = 'Account';
  static const String accountPage = 'Account Page';

  // List View
  static const String addClass = 'Add Class';
  static const String addStudent = 'Add Student';
  static const String attendance = 'Check Attendance';
  static const String help = 'Help';
  static const String about = 'About';

  // Attendance check page
  static const String attendanceCheckPage = 'Attendance Check Page';

  // Add Class page
    static const String addClassPage = 'Add Class Page';
  
  // Developer menu
  static const String developerMenu = 'Developer menu';
  static const String authenticationType = 'Authentication type';
  static const String firebase = 'Firebase';
  static const String mock = 'Mock';

  // Name of App
  static const String name = 'JARASADI';

  // Message Body
  static const String body =
      'Greetings! This is to inform you that your child attended the class';
}