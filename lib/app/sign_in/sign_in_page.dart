import 'dart:async';

import 'package:dsc_project/app/sign_in/email_password/email_password_sign_in_page.dart';
import 'package:dsc_project/app/sign_in/sign_in_button.dart';
import 'package:dsc_project/common_widgets/platform_exception_alert_dialog.dart';
import 'package:dsc_project/constants/keys.dart';
import 'package:dsc_project/constants/strings.dart';
import 'package:dsc_project/services/flutter_sms_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:dsc_project/services/firebase_auth_service.dart';
import 'package:dsc_project/app/sign_in/sign_in_view_model.dart';

class SignInPageBuilder extends StatelessWidget {
  final FlutterSmsService smsService = FlutterSmsService();
    
  @override
  Widget build(BuildContext context) {
    final FirebaseAuthService auth =
        Provider.of<FirebaseAuthService>(context, listen: false);
    return ChangeNotifierProvider<SignInViewModel>(
      create: (_) => SignInViewModel(auth: auth),
      child: Consumer<SignInViewModel>(
        builder: (_, SignInViewModel viewModel, __) => SignInPage._(
          viewModel: viewModel,
        ),
      ),
    );
  }
}

class SignInPage extends StatelessWidget {
  const SignInPage._({Key key, this.viewModel, this.title}) : super(key: key);
  final SignInViewModel viewModel;
  final String title;

  static const Key emailPasswordButtonKey = Key(Keys.emailPassword);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
       Container (
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/opening_background.png'),
              fit: BoxFit.cover)
          ),
        child: _buildSignIn(context),
        ),
    );
  }

  Widget _buildHeader() {
    if (viewModel.isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    return Container(
      decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/logo.png'),
              )
          )
    );
  }

  Widget _buildSignIn(BuildContext context) {
    // Make content scrollable so that it fits on small screens
    return Container(
      padding: EdgeInsets.all(0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 300.0,
            child: _buildHeader(),
          ),
          SizedBox(
            height: 10,
          ),
          FlatButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
            onPressed: viewModel.isLoading
                ? null
                : () => EmailPasswordSignInPageBuilder.show(context),
            child: Image.asset('assets/images/get_started_button.png')
          )
        ],
      ),
    );
  }
}