import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:dsc_project/services/database_services.dart';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class CRUDViewModel extends ChangeNotifier {
  var _apiClasses = Api(path: 'classes');
  var _apiStudents = Api(path: 'students');
  var _apiStudentRecords = Api(path: 'student_records');
  List<Class> classes;
  List<Student> students;
  DocumentSnapshot _doc;
  DocumentSnapshot get doc => _doc;

  Future getStudentRecords() async {
    return await _apiStudentRecords.getDataCollection();
  }

  Future getStudentData(String studentId) async {
    return await _apiStudents.getDocumentById(studentId);
  }

  Stream<QuerySnapshot> fetchCollectionAsStream(
      String path, String field, String id) {
    return Api(path: path).streamDataCollection(path, field, id);
  }

  Future getClassData(String classId) async {
    _doc = await _apiClasses.getDocumentById(classId);
  }

  Future deleteClass(String classId) async {
    _doc = await _apiClasses.deleteDocumentById(classId);
  }

  Future updateClass(String className, String classSection, String startTime,
      String endTime, String id) async {
    return await _apiClasses.updateClassDocument(
        className, classSection, startTime, endTime, id);
  }

  void setData() {
    appData.className = doc['className'];
    appData.classSection = doc['classSection'];
    appData.startTime = doc['startTime'];
    appData.endTime = doc['endTime'];
  }

  Future updateStudentRecord(String status, int statusRecord, String studentRecordId) async {
    return await _apiStudentRecords.updateStudentRecordDocument(status,
        statusRecord, studentRecordId);
  }
}