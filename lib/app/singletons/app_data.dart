import 'package:cloud_firestore/cloud_firestore.dart';

class AppData {
  static final AppData _appData = AppData._internal();

  String classId;
  String className = '';
  String classSection = '';
  String startTime = '';
  String endTime = '';
  Timestamp clockedIn;
  String studentId;

  factory AppData() {
    return _appData;
  }

  AppData._internal();
}

final appData = AppData();