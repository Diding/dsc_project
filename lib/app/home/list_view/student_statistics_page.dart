import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:dsc_project/app/view_models/crud_view_model.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pie_chart/pie_chart.dart';

class StudentStatisticsPageBuilder extends StatelessWidget {
  const StudentStatisticsPageBuilder({Key key, this.onStudentStatisticsPage})
      : super(key: key);
  final VoidCallback onStudentStatisticsPage;

  static Future<void> showPage(BuildContext context) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.studentStatisticsPageBuilder,
        arguments: StudentStatisticsPageBuilderArguments(
          onStudentStatisticsPage: () => navigator.pop(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CRUDViewModel>(
      create: (_) => CRUDViewModel(),
      child: Consumer<CRUDViewModel>(
        builder: (_, CRUDViewModel model, __) => StudentStatisticsPage(
          model: model,
          onStudentStatisticsPage: onStudentStatisticsPage,
        ),
      ),
    );
  }
}

class StudentStatisticsPage extends StatefulWidget {
  const StudentStatisticsPage(
      {Key key, @required this.model, this.onStudentStatisticsPage})
      : super(key: key);
  final CRUDViewModel model;
  final VoidCallback onStudentStatisticsPage;

  @override
  _StudentStatisticsPageState createState() => _StudentStatisticsPageState();
}

class _StudentStatisticsPageState extends State<StudentStatisticsPage> {
  Widget _buildPieGraph(
      BuildContext context, presentRecord, absentRecord, lateRecord) {
    Map<String, double> dataMap = Map();

    dataMap.putIfAbsent('Present', () => presentRecord.toDouble());
    dataMap.putIfAbsent('Absent', () => absentRecord.toDouble());
    dataMap.putIfAbsent('Late', () => lateRecord.toDouble());

    return PieChart(
      colorList: [Colors.greenAccent[400], Colors.red, Colors.blueAccent],
      dataMap: dataMap,
      legendStyle: TextStyle(color: Colors.grey, fontFamily: 'Open Sans', fontSize: 12.0),
      animationDuration: Duration(milliseconds: 1000),
      chartLegendSpacing: 32.0,
      chartRadius: MediaQuery.of(context).size.width / 2.7,
      showChartValuesInPercentage: true,
      showChartValues: true,
      chartValueStyle: TextStyle(color: Colors.black),
    );
  }

  _buildPopUp(BuildContext context, presentRecord, absentRecord, lateRecord) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Attendance Record Statistics', style: TextStyle(fontFamily: 'Open Sans', fontWeight: FontWeight.w600)),
                    SizedBox(height: 10.0),
                    Text('Total Attendance: ${presentRecord + absentRecord + lateRecord}', style: TextStyle(fontSize: 12.0, color: Colors.teal, letterSpacing: 1.0)),
                    SizedBox(height: 10.0),
                    _buildPieGraph(
                        context, presentRecord, absentRecord, lateRecord),
                    SizedBox(height: 8.0),
                    Text('Present - $presentRecord  |  Absent - $absentRecord  |  Late - $lateRecord', style: TextStyle(fontSize: 12.0, color: Colors.grey[600], fontWeight: FontWeight.w500)),
                  ],
                );
              },
            ),
          );
        }
      );
  }

  Widget _buildStats(BuildContext context) {
    final studentRecordsProvider =
        Provider.of<CRUDViewModel>(context, listen: false);
    QuerySnapshot studentRecords;

    _getStudentRecord() {
      studentRecordsProvider.getStudentRecords().then((records) {
        studentRecords = records;

        for (var i = 0; i < studentRecords.documents.length; i++) {
          var docs = studentRecords.documents[i];

          if (docs.data['studentId'] == appData.studentId) {
            _buildPopUp(context, docs.data['presentRecord'],
                docs.data['absentRecord'], docs.data['lateRecord']);
          }
        }
      });
    }
    return _getStudentRecord();
  }

  Widget _buildCardTile(AsyncSnapshot<dynamic> snapshot, BuildContext context) {
    List students;
    List<ListTile> list = List<ListTile>();

    students = snapshot.data.documents
        .map((doc) => Student.fromMap(doc.data, doc.documentID))
        .toList();

    for (var i = 0; i < students.length; i++) {
      var docs = students[i];
      list.add(ListTile(
        title: Text('${docs.lastname}, ${docs.firstname}',
            style: TextStyle(color: Colors.black, letterSpacing: 1.2, fontWeight: FontWeight.w500)),
        trailing: Icon(Icons.arrow_right, size: 30, color: Colors.teal),
        onTap: () {
          appData.studentId = docs.studentId;
          _buildStats(context);
        },
      ));
    }

    if (list.length > 0) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: list.length,
              itemBuilder: (context, index) {
                return Card(
                    margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
                    color: Colors.grey[300],
                    child: list[index]);
              },
            )
          ],
        ),
      );
    } else {
      return Container(
        child: Center(
          child: Text("NO STATISTICS TO SHOW"),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final studentProvider = Provider.of<CRUDViewModel>(context);
    final classId = appData.classId;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text('Statistics', style: TextStyle(fontSize: 18)),
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: StreamBuilder(
              stream: studentProvider.fetchCollectionAsStream(
                  'students', 'classId', classId),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return _buildCardTile(snapshot, context);
                } else {
                  return Container(
                    child: Center(
                      child: Text("NO STUDENTS YET"),
                    ),
                  );
                }
              }
            )
          ),
    );
  }
}