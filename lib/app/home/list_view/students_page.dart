import 'package:dsc_project/app/home/list_view/add_student_page.dart';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:dsc_project/app/view_models/crud_view_model.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StudentsPageBuilder extends StatelessWidget {
  const StudentsPageBuilder({Key key, this.onStudentsPage}) : super(key: key);
  final VoidCallback onStudentsPage;

  static Future<void> showPage(BuildContext context) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.studentsPageBuilder,
        arguments: StudentsPageBuilderArguments(
          onStudentsPage: () => navigator.pop(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CRUDViewModel>(
      create: (_) => CRUDViewModel(),
      child: Consumer<CRUDViewModel>(
        builder: (_, CRUDViewModel model, __) => StudentsPage(
          model: model,
          onStudentsPage: onStudentsPage,
        ),
      ),
    );
  }
}

class StudentsPage extends StatefulWidget {
  const StudentsPage({Key key, @required this.model, this.onStudentsPage})
      : super(key: key);
  final CRUDViewModel model;
  final VoidCallback onStudentsPage;

  @override
  _StudentsPageState createState() => _StudentsPageState();
}

class _StudentsPageState extends State<StudentsPage> {
  
  Widget _buildCardTile(AsyncSnapshot<dynamic> snapshot, BuildContext context) {
    List students;
    List<ListTile> list = List<ListTile>();

    students = snapshot.data.documents
        .map((doc) => Student.fromMap(doc.data, doc.documentID))
        .toList();

    for (var i = 0; i < students.length; i++) {
      var docs = students[i];
      list.add(ListTile(
        leading: Icon(Icons.person_outline, color: Colors.teal, size: 50),
        title: Text('${docs.lastname}, ${docs.firstname}',
            style: TextStyle(color: Colors.black, letterSpacing: 1.2, fontWeight: FontWeight.w500)),
        subtitle: Text('${docs.parentContactNumber}'),
      ));
    }

    if (list.length > 0) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: list.length,
              itemBuilder: (context, index) {
                return list[index];
              },
            )
          ],
        ),
      );
    } else {
      return Container(
        child: Center(
          child: Text("NO STATISTICS TO SHOW"),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final studentProvider = Provider.of<CRUDViewModel>(context);
    final classId = appData.classId;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text('Students', style: TextStyle(fontSize: 18)),
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: StreamBuilder(
              stream: studentProvider.fetchCollectionAsStream(
                  'students', 'classId', classId),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return _buildCardTile(snapshot, context);
                } else {
                  return Container(
                    child: Center(
                      child: Text("NO STUDENTS YET"),
                    ),
                  );
                }
              })),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          AddStudentPageBuilder.showPage(context);
        },
        child: Icon(Icons.person_add),
      ),
    );
  }
}
