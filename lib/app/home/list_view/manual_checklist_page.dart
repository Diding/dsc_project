import 'dart:async';
import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dsc_project/app/home/home_page.dart';
import 'package:dsc_project/common_widgets/form_submit_button.dart';
import 'package:dsc_project/common_widgets/pop_up_message.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:dsc_project/services/attendance_services.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:dsc_project/app/view_models/crud_view_model.dart';
import 'package:dsc_project/services/flutter_sms_service.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ManualChecklistPageBuilder extends StatelessWidget {
  const ManualChecklistPageBuilder({Key key, this.onChecklistPage})
      : super(key: key);
  final VoidCallback onChecklistPage;

  static Future<void> showPage(BuildContext context) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.manualChecklistPageBuilder,
        arguments: ManualChecklistPageBuilderArguments(
          onChecklistPage: () => navigator.pop(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CRUDViewModel>(
      create: (_) => CRUDViewModel(),
      child: Consumer<CRUDViewModel>(
        builder: (_, CRUDViewModel model, __) => ManualChecklistPage(
          model: model,
          onChecklistPage: onChecklistPage,
        ),
      ),
    );
  }
}

class ManualChecklistPage extends StatefulWidget {
  const ManualChecklistPage(
      {Key key, @required this.model, this.onChecklistPage})
      : super(key: key);
  final CRUDViewModel model;
  final VoidCallback onChecklistPage;

  @override
  _ManualChecklistPageState createState() => _ManualChecklistPageState();
}

class Status {
  int id;
  String status;
  Status({this.id, this.status});

  static List<Status> getStatus() {
    return <Status>[
      Status(id: 1, status: 'Present'),
      Status(id: 2, status: 'Absent'),
      Status(id: 3, status: 'Late'),
    ];
  }
}

class _ManualChecklistPageState extends State<ManualChecklistPage> {
  AttendanceServices newAttendanceService = AttendanceServices();
  FlutterSmsService smsService = FlutterSmsService();

  List<Status> status = Status.getStatus();
  List presentStudents = [];
  List absentStudents = [];
  List lateStudents = [];
  List attendance = [];
  int presentRecord = 0;
  int absentRecord = 0;
  int lateRecord = 0;
  var selectedStatus;

  @override
  void initState() {
    super.initState();
    widget.model.getClassData(appData.classId).then((mod) {
      setState(() {
        widget.model.setData();
      });
    });
  }

  Widget _buildTile(AsyncSnapshot<dynamic> snapshot, BuildContext context) {
    List<DropdownMenuItem> statusItems = [];
    List students;
    List<ListTile> list = List<ListTile>();

    for (int i = 0; i < status.length; i++) {
      statusItems.add(DropdownMenuItem(
        child: Text('${status[i].status}', style: TextStyle(color: Colors.teal, fontWeight: FontWeight.w400)),
        value: "${status[i].status}",
      ));
    }

    students = snapshot.data.documents
        .map((doc) => Student.fromMap(doc.data, doc.documentID))
        .toList();

    for (var i = 0; i < students.length; i++) {
      var docs = students[i];
      list.add(ListTile(
        title: Text('${docs.lastname}, ${docs.firstname}',
            style: TextStyle(color: Colors.black, letterSpacing: 1.2, fontWeight: FontWeight.w500)),
        trailing: DropdownButton(
          items: statusItems,
          onChanged: (statusValue) {
            setState(() {
              selectedStatus = statusValue;

              if (selectedStatus == 'Present') {
                presentRecord += 1;
                presentStudents.add(docs.studentId);
              } else if (selectedStatus == 'Absent') {
                absentRecord += 1;
                absentStudents.add(docs.studentId);
              } else if (selectedStatus == 'Late') {
                lateRecord += 1;
                lateStudents.add(docs.studentId);
              }

              String str = jsonEncode(Attendance(
                  student: jsonEncode(Student(
                      firstname: docs.firstname,
                      lastname: docs.lastname,
                      parentContactNumber: docs.parentContactNumber)),
                  status: selectedStatus));
              attendance.add(str);
            });
          },
          value: selectedStatus,
          isExpanded: false,
        ),
      ));
    }

    if (list.length > 0) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: list.length,
              itemBuilder: (context, index) {
                return Card(
                    margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
                    color: Colors.grey[200],
                    child: list[index]);
              },
            ),
            SizedBox(height: 50),
            _buildSubmitButton(context)
          ],
        ),
      );
    } else {
      return Container(
        child: Center(
          child: Text("NO STUDENTS YET"),
        ),
      );
    }
  }

  Widget _buildSubmitButton(BuildContext context) {
    final studentRecordsProvider = Provider.of<CRUDViewModel>(context);
    final studentDataProvider = Provider.of<CRUDViewModel>(context);
    var date = DateFormat('yyyy-MM-dd').format(appData.clockedIn.toDate());
    var time = DateFormat('h:mm a').format(appData.clockedIn.toDate());
    QuerySnapshot studentRecords;

    _updateFunction(String dataField, DocumentSnapshot docs) {
      var record = docs.data['$dataField'];
      studentRecordsProvider.updateStudentRecord(
          '$dataField', record += 1, docs.documentID);
    }

    _generateMessageBody(String firstname, String lastname, String status) {
      var body =
          'Good Day! This is to inform you that $firstname $lastname is $status in ${appData.className} Class today ($date at $time). Yours truly, ${appData.className} teacher';
      return body;
    }

    _sendNotifUsingStudentData(String studentId, String status) {
      var data = studentDataProvider.getStudentData(studentId);
      data.then((doc) {
        var firstname = doc.data['firstname'];
        var lastname = doc.data['lastname'];
        var parentContactNumber = doc.data['parentContactNumber'];

        var messageBody = _generateMessageBody(firstname, lastname, status);
        Future.delayed(
          Duration(seconds: 10),
          () {
            smsService.sendMessage(parentContactNumber, messageBody);
          },
        );
      });
    }

    return FormSubmitButton(
      text: 'Record',
      onPressed: () async {
        String attendanceJson = jsonEncode(attendance);

        try {
          studentRecordsProvider.getStudentRecords().then((records) {
            studentRecords = records;

            for (var i = 0; i < studentRecords.documents.length; i++) {
              var docs = studentRecords.documents[i];

              if (presentStudents.contains(docs.data['studentId'])) {
                _updateFunction('presentRecord', docs);
                _sendNotifUsingStudentData(docs.data['studentId'], 'present');
              } else if (absentStudents.contains(docs.data['studentId'])) {
                _updateFunction('absentRecord', docs);
                _sendNotifUsingStudentData(docs.data['studentId'], 'absent');
              } else if (lateStudents.contains(docs.data['studentId'])) {
                _updateFunction('lateRecord', docs);
                _sendNotifUsingStudentData(docs.data['studentId'], 'late');
              }
            }
          });

          await newAttendanceService.submitAttendance(
              appData.classId, appData.clockedIn, attendanceJson);
          Future.delayed(
            Duration(seconds: 2),
            () {
              PopUpMessage().popUpMessage('Attendance successfully submitted',
                  'Class ID: ${appData.classId} \n Date: $date', () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomePage()));
              }, context);
            },
          );
        } catch (e) {
          Future.delayed(
            Duration(seconds: 2),
            () {
              PopUpMessage().popUpMessage(
                  'Sorry! an ERROR occurred', 'Error details: ${e.toString()}',
                  () {
                ManualChecklistPageBuilder.showPage(context);
              }, context);
            },
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final studentProvider = Provider.of<CRUDViewModel>(context);
    final classId = appData.classId;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text(
            '${appData.className} ${appData.classSection} (${appData.startTime} - ${appData.endTime})',
            style: TextStyle(fontSize: 18)),
          ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: StreamBuilder(
              stream: studentProvider.fetchCollectionAsStream(
                  'students', 'classId', classId),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return _buildTile(snapshot, context);
                } else {
                  return Container(
                    child: Center(
                      child: Text("NO STUDENTS YET"),
                    ),
                  );
                }
              }
            )
          ),
    );
  }
}