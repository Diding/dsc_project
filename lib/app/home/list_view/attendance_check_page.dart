import 'dart:async';
import 'package:dsc_project/app/home/list_view/manual_checklist_page.dart';
import 'package:dsc_project/common_widgets/form_submit_button.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:dsc_project/services/firebase_auth_service.dart';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:dsc_project/app/view_models/crud_view_model.dart';
import 'package:flutter/material.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:dsc_project/constants/strings.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class AttendanceCheckPageBuilder extends StatelessWidget {
  const AttendanceCheckPageBuilder({Key key, this.onAttendancePage})
      : super(key: key);
  final VoidCallback onAttendancePage;

  static Future<void> showPage(BuildContext context) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.attendanceCheckPageBuilder,
        arguments: AttendanceCheckPageBuilderArguments(
          onAttendancePage: () => navigator.pop(),
        )
      );
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CRUDViewModel>(
      create: (_) => CRUDViewModel(),
      child: Consumer<CRUDViewModel>(
        builder: (_, CRUDViewModel model, __) => AttendanceCheckPage(
            model: model, onAttendancePage: onAttendancePage),
      ),
    );
  }
}

class Choice {
  int id;
  String value;
  Choice({this.id, this.value});

  static List<Choice> getChoices() {
    return <Choice>[
      Choice(id: 1, value: "Checklist"),
      Choice(id: 2, value: "Face Recognition"),
    ];
  }
}

class AttendanceCheckPage extends StatefulWidget {
  const AttendanceCheckPage(
      {Key key, @required this.model, this.onAttendancePage})
      : super(key: key);
  final CRUDViewModel model;
  final VoidCallback onAttendancePage;

  @override
  _AttendanceCheckPageState createState() => _AttendanceCheckPageState();
}

class _AttendanceCheckPageState extends State<AttendanceCheckPage> {
  List<Choice> choices = Choice.getChoices();
  int _currentChoiceValue = 0;
  var selectedClass;

  Widget _buildDateLabel() {
    DateTime now = DateTime.now();
    String date = DateFormat('EEE, M/d/y').format(now);
    return Container(
        margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
        width: 350,
        height: 150,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.black26,
        ),
        child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ListTile(
                leading:
                    Icon(Icons.calendar_today, size: 70, color: Colors.teal),
                title: Text(date,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30.0,
                        color: Colors.grey[600])),
              ),
            ],
          ),
        )
      );
  }

  Widget _buildDropdownMenu(AsyncSnapshot<dynamic> snapshot) {
    List classes;
    List<DropdownMenuItem> classItems = [];
    classes = snapshot.data.documents
        .map((doc) => Class.fromMap(doc.data, doc.documentID))
        .toList();

    for (int i = 0; i < classes.length; i++) {
      var docs = classes[i];
      classItems.add(DropdownMenuItem(
        child: Text('${docs.className} ${docs.classSection}',
            style: TextStyle(color: Colors.teal[700])),
        value: "${docs.id}",
      ));
    }

    if (classItems.length > 0) {
      return Container(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 10),
              _buildDateLabel(),
              SizedBox(height: 70),
              Text("SELECT YOUR CLASS",
                  style: TextStyle(fontSize: 15, color: Colors.grey[700])),
              SizedBox(height: 10),
              DropdownButton(
                items: classItems,
                onChanged: (classValue) {
                  setState(() {
                    selectedClass = classValue;
                  });
                  appData.classId = selectedClass;
                },
                value: selectedClass,
                isExpanded: false,
              ),
              SizedBox(height: 60),
              _buildSubmitButton(),
            ],
          ),
        ),
      );
    } else {
      return Container(
        child: Center(
          child: Text('NO CLASSES YET'),
        ),
      );
    }
  }

  Widget _buildSubmitButton() {
    return FormSubmitButton(
      text: 'Check',
      onPressed: () {
        _buildPopUpMenu(context);
      },
    );
  }

  _buildPopUpMenu(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: List<Widget>.generate(2, (int index) {
                  return RadioListTile(
                    title: Text(choices[index].value),
                    value: choices[index].id,
                    groupValue: _currentChoiceValue,
                    onChanged: (value) {
                      setState(() {
                        _currentChoiceValue = value;
                      });
                    },
                  );
                }),
              );
            }),
            actions: _buildActions(),
          );
        });
  }

  List<Widget> _buildActions() {
    List<Widget> actions = List<Widget>();

    actions.add(FlatButton(
      child: Text('CANCEL'),
      onPressed: () {
        Navigator.of(context).pop();
      },
    ));
    actions.add(FlatButton(
      child: Text('OK'),
      onPressed: () {

        if (_currentChoiceValue == 1) {
          ManualChecklistPageBuilder.showPage(context);
        } else if (_currentChoiceValue == 2) {
        }
      },
    ));
    return actions;
  }

  @override
  Widget build(BuildContext context) {
    final classProvider = Provider.of<CRUDViewModel>(context);
    final user = Provider.of<User>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text(Strings.attendanceCheckPage),
        ),
        resizeToAvoidBottomPadding: false,
        body: Container(
          color: Colors.white,
          height: MediaQuery.of(context).size.height - 80,
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: StreamBuilder(
              stream: classProvider.fetchCollectionAsStream(
                  'classes', 'uid', user.uid),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return _buildDropdownMenu(snapshot);
                } else {
                  return Text('');
                }
              }
            ),
        )
      );
  }
}