import 'package:dsc_project/app/home/home_page.dart';
import 'package:dsc_project/app/home/list_view/add_class_view_model.dart';
import 'package:dsc_project/app/home/list_view/validator.dart';
import 'package:dsc_project/common_widgets/custom_tile.dart';
import 'package:dsc_project/common_widgets/form_submit_button.dart';
import 'package:dsc_project/common_widgets/pop_up_message.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:dsc_project/services/firebase_auth_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dsc_project/services/class_services.dart';

class ClassFieldValidator {
  static String validateClassSection(String value) {
    return value.isEmpty ? 'Class Section must not be blank' : null;
  }

   static String validateClassName(String value) {
    return value.isEmpty ? 'Class Name must not be blank' : null;
  }
}

class AddClassPageBuilder extends StatelessWidget {
  const AddClassPageBuilder({Key key, this.onClassPage}) : super(key: key);
  final VoidCallback onClassPage;

  static Future<void> showPage(BuildContext context) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.addClassPageBuilder,
        arguments: AddClassPageBuilderArguments(
          onClassPage: () => navigator.pop(),
        ));
  }

  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AddClassPageViewModel>(
      create: (_) => AddClassPageViewModel(),
      child: Consumer<AddClassPageViewModel>(
        builder: (_, AddClassPageViewModel model, __) =>
            AddClassPage(model: model, onClassPage: onClassPage),
      ),
    );
  }
}

class AddClassPage extends StatefulWidget {
  const AddClassPage({Key key, @required this.model, this.onClassPage})
      : super(key: key);
  final AddClassPageViewModel model;
  final VoidCallback onClassPage;

  @override
  _AddClassPageState createState() => _AddClassPageState();
}

class _AddClassPageState extends State<AddClassPage> {
  ClassServices newClassService = ClassServices();
  final databaseReference = Firestore.instance;
  final formKey = GlobalKey<FormState>();
  String classSection;
  String subjectName;
  TimeOfDay currentTime = TimeOfDay.now();
  TimeOfDay timePicked;
  String end = '';
  String start = '';

  Future<void> startTime(BuildContext context) async {
    timePicked = await showTimePicker(
      context: context,
      initialTime: currentTime,
    );

    MaterialLocalizations localizations = MaterialLocalizations.of(context);
    String formattedTime =
        localizations.formatTimeOfDay(timePicked, alwaysUse24HourFormat: true);
    setState(() {
      start = formattedTime;
    });
  }

  Future<void> endTime(BuildContext context) async {
    timePicked = await showTimePicker(
      context: context,
      initialTime: currentTime,
    );

    MaterialLocalizations localizations = MaterialLocalizations.of(context);
    String formattedTime =
        localizations.formatTimeOfDay(timePicked, alwaysUse24HourFormat: true);
    setState(() {
      end = formattedTime;
    });
  }

  Widget _inputField(String title) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 3),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            style: TextStyle(
                color: Colors.teal[700],
                fontFamily: 'Open Sans',
                fontSize: 18.0),
            onChanged: (value) {
              title == 'Class Section'
                  ? classSection = value
                  : subjectName = value;
            },
            validator: (value) {
              if (title == 'Class Section') {
                return FieldValidator.validateClassSection(value);
              } else {
                return FieldValidator.validateClassName(value);
              }
            },
            decoration: InputDecoration(
              labelText: title,
              labelStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                  fontFamily: 'Open Sans',
                  fontSize: 18.0),
            ),
          ),
        ],
      ),
    );
  }

  Widget _inputWidget() {
    return Container(
      margin: EdgeInsets.all(0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 10.0),
          _inputField('Class Section'),
          _inputField('Class Name'),
          SizedBox(height: 10.0),
        ],
      ),
    );
  }

  Widget _listTile(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        CustomListTile(
          icon: Icons.access_time,
          text: 'Start Time',
          onTap: () {
            startTime(context);
          },
          label: '$start',
        ),
        CustomListTile(
          icon: Icons.access_time,
          text: 'End Time',
          onTap: () {
            endTime(context);
          },
          label: '$end',
        ),
      ],
    );
  }

  Widget _submitButton() {
    final user = Provider.of<User>(context);
    String uid = user.uid;
    return FormSubmitButton(
        text: 'Create New Class',
        onPressed: () async {
          if (formKey.currentState.validate()) {
            formKey.currentState.save();

            try {
              await newClassService.createClass(
                  classSection, subjectName, start, end, uid);
              Future.delayed(
                Duration(seconds: 1),
                () {
                  PopUpMessage().popUpMessage(
                      'Class successfully added',
                      'Class Name: $subjectName \n Class Section: $classSection',
                      () => Navigator.push(context,
                          MaterialPageRoute(builder: (context) => HomePage())),
                      context);
                },
              );
            } catch (e) {
              Future.delayed(
                Duration(seconds: 2),
                () {
                  PopUpMessage().popUpMessage(
                      'Sorry! an ERROR occurred',
                      'Error details: ${e.toString()}',
                      () => AddClassPageBuilder.showPage(context),
                      context);
                },
              );
            }
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("New Class"),
      ),
      resizeToAvoidBottomPadding: false,
      body: Form(
        key: formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              color: Colors.white,
              height: MediaQuery.of(context).size.height - 80,
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 20.0),
                  _inputWidget(),
                  _listTile(context),
                  SizedBox(height: 50.0),
                  _submitButton(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
