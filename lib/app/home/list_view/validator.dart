class FieldValidator {

  static String validateClassSection(String value) {
    return value.isEmpty ? 'Class Section must not be blank' : null;
  }

   static String validateClassName(String value) {
    return value.isEmpty ? 'Class Name must not be blank' : null;
  }
  
  static String validateFirstName(String value) {
    if (value.isEmpty) return 'Enter First Name';

    return null;
  }

  static String validateLastName(String value) {
    if (value.isEmpty) return 'Enter Last Name';

    return null;
  }

  static String validPhoneNumber(String value) {
    if (value.isEmpty) return 'Enter Contact Number';

    Pattern pattern = r'(09|\+639)\d{9}';

    RegExp regex = new RegExp(pattern);

    if (!regex.hasMatch(value)) {
      return 'Enter Valid Number';
    }

    return null;
  }
}