import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dsc_project/app/home/home_page.dart';
import 'package:dsc_project/app/home/list_view/class_page_view_model.dart';
import 'package:dsc_project/app/home/list_view/manual_checklist_page.dart';
import 'package:dsc_project/app/home/list_view/records_page.dart';
import 'package:dsc_project/app/home/list_view/student_statistics_page.dart';
import 'package:dsc_project/app/home/list_view/students_page.dart';
import 'package:dsc_project/app/home/list_view/update_class_page.dart';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:dsc_project/common_widgets/pop_up_message.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:flutter/material.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:dsc_project/app/view_models/crud_view_model.dart';

class ClassPageBuilder extends StatelessWidget {
  final Class classInfo;
  const ClassPageBuilder({Key key, this.onViewClassPage, this.classInfo})
      : super(key: key);

  final VoidCallback onViewClassPage;

  static Future<void> showPage(BuildContext context, Class classInfo) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.classPageBuilder,
        arguments: ClassPageBuilderArguments(
            onViewClassPage: () => navigator.pop(), classInfo: classInfo));
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ClassPageViewModel>(
      create: (_) => ClassPageViewModel(),
      child: Consumer<ClassPageViewModel>(
        builder: (_, ClassPageViewModel model, __) => ClassPage(
          model: model,
          onViewClassPage: onViewClassPage,
          classInfo: classInfo,
        ),
      ),
    );
  }
}

class ClassPage extends StatefulWidget {
  const ClassPage(
      {Key key, @required this.model, this.onViewClassPage, this.classInfo})
      : super(key: key);
  final ClassPageViewModel model;
  final VoidCallback onViewClassPage;
  final Class classInfo;

  @override
  _ClassPageState createState() => _ClassPageState(classInfo: classInfo);
}

class PopupMenuOption extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class CustomPopupMenu {
  CustomPopupMenu({this.title, this.icon});
  String title;
  IconData icon;
}

List<CustomPopupMenu> choices = <CustomPopupMenu>[
  CustomPopupMenu(title: 'Update', icon: Icons.update),
  CustomPopupMenu(title: 'Delete', icon: Icons.delete),
];

class _ClassPageState extends State<ClassPage> {
  Class classInfo;
  _ClassPageState({this.classInfo});

  String date = DateFormat('EEE, M/d/y').format(DateTime.now());
  String clockInStatus = 'Clock In';
  String clockedIn = 'Not Clocked In';

  void _deleteClass(classId) {
    CRUDViewModel classProvider = CRUDViewModel();
    classProvider.deleteClass(classId);
  }

  Future<void> _deleteAction(classId) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Delete class'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(''),
                Text('Once delete cannot be recovered'),
                Text(''),
                Text('Are you sure you want to delete this class?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Delete"),
              onPressed: () {
                _deleteClass(classId);

                Future.delayed(
                  Duration(seconds: 2),
                  () {
                    PopUpMessage().popUpMessage(
                        '',
                        'Class was deleted successfully',
                        () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomePage())),
                        context);
                  },
                );
              },
            ),
          ],
        );
      },
    );
  }

  _buildAttendanceChoices(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Choose Method: ',
                      style: TextStyle(
                          fontFamily: 'Open Sans',
                          fontSize: 18.0,
                          fontWeight: FontWeight.w600)),
                  SizedBox(height: 15.0),
                  Card(
                      color: Colors.black,
                      child: InkWell(
                        onTap: () {
                          if (clockedIn == 'Not Clocked In') {
                            PopUpMessage().popUpMessage(
                                'Oh No!', "You forgot to clock in", () {
                              Navigator.of(context).pop();
                              ClassPageBuilder.showPage(context, classInfo);
                            }, context);
                          } else {
                            appData.classId = classInfo.id;
                            ManualChecklistPageBuilder.showPage(context);
                          }
                        },
                        child: ListTile(
                          contentPadding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                          leading: Icon(Icons.check,
                              size: 30, color: Colors.teal),
                          title: Text("Checklist",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 15.0,
                                  color: Colors.white,
                                  letterSpacing: 1.2)),
                        ),
                      )),
                  Card(
                      color: Colors.black,
                      child: InkWell(
                        onTap: () {
                          // face recog nadi dapat
                        },
                        child: ListTile(
                          contentPadding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                          leading: Icon(Icons.camera_alt,
                              size: 30, color: Colors.teal),
                          title: Text("Face Recognition",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 15.0,
                                  color: Colors.white,
                                  letterSpacing: 1.2)),
                        ),
                      )
                    ),
                ],
              );
            }),
          );
        }
      );
  }

  Widget _buildCardClockIn(BuildContext context) {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
        padding: EdgeInsets.fromLTRB(15, 5, 0, 5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.black38,
        ),
        width: MediaQuery.of(context).size.width - 35,
        height: MediaQuery.of(context).size.width / 3.0,
        child: Row(children: <Widget>[
          Column(children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Text(
              'Clock In',
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Open Sans',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 1.2),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "You haven't clock in yet",
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Open Sans',
                fontSize: 12,
                fontWeight: FontWeight.w400,
                letterSpacing: 1.3,
              ),
            )
          ]),
          SizedBox(width: 40),
          Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(100)),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(100)),
                onPressed: () {
                  setState(() {
                    clockedIn = 'Clocked In';
                    clockInStatus = clockedIn;
                    appData.clockedIn = Timestamp.now();
                  });
                },
                child:
                    Text(clockInStatus, style: TextStyle(color: Colors.white)),
              ))
        ]));
  }

  Widget _buildCardOptions(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 5, 0, 10),
      padding: EdgeInsets.fromLTRB(0, 15, 0, 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
      ),
      width: MediaQuery.of(context).size.width - 35,
      height: MediaQuery.of(context).size.height / 1.8,
      child: _buildGrid(context),
    );
  }

  Widget _buildIconButtons(
      BuildContext context, String title, IconData icon, onTap) {
    return Card(
      color: Colors.transparent,
      child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: Colors.grey[700],
                width: 3.0
              )
            ),
          child: InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: onTap,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Icon(icon, color: Colors.teal, size: 60),
                  Text(title,
                      style: TextStyle(
                        letterSpacing: 1.3,
                          fontSize: 12,
                          color: Colors.black,
                          fontFamily: 'Open Sans',
                          fontWeight: FontWeight.w600))
                ],
              ),
            ),
          )),
    );
  }

  Widget _buildGrid(BuildContext context) {
    List<Card> iconButtons = [
      _buildIconButtons(context, 'STUDENTS', Icons.person, () => StudentsPageBuilder.showPage(context)),
      _buildIconButtons(context, 'ATTENDANCE', Icons.calendar_today,
          () => _buildAttendanceChoices(context)),
      _buildIconButtons(context, 'RECORDS', Icons.book,
          () => RecordsPageBuilder.showPage(context)),
      _buildIconButtons(context, 'STATISTICS', Icons.assessment,
          () => StudentStatisticsPageBuilder.showPage(context)),
    ];
    return CustomScrollView(slivers: <Widget>[
      SliverGrid(
        delegate: SliverChildBuilderDelegate(
          (context, index) {
            return Container(
              alignment: Alignment.center,
              child: iconButtons[index],
            );
          },
          childCount: iconButtons.length,
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 15,
          crossAxisSpacing: 15,
          childAspectRatio: 1,
        ),
      )
    ]);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Column(children: <Widget>[
                Stack(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      child: Container(
                        height: MediaQuery.of(context).size.width / 3,
                        width: MediaQuery.of(context).size.width / 3,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100.0),
                          border: Border.all(
                              color: Colors.black,
                              style: BorderStyle.solid,
                              width: MediaQuery.of(context).size.width / 3),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.close),
                            iconSize: 23.0,
                            color: Colors.black,
                            onPressed: () => Navigator.pop(context),
                          ),
                          PopupMenuButton<String>(
                              elevation: 30,
                              onSelected: (value) {
                                if (value == "Delete") {
                                  _deleteAction(classInfo.id);
                                }
                                if (value == "Update") {
                                  appData.classSection = classInfo.classSection;
                                  appData.className = classInfo.className;
                                  appData.startTime = classInfo.startTime;
                                  appData.endTime = classInfo.endTime;
                                  UpdateClassPageBuilder.showPage(
                                      context, classInfo);
                                }
                              },
                              itemBuilder: (context) {
                                return choices.map((CustomPopupMenu choice) {
                                  return PopupMenuItem<String>(
                                    value: choice.title,
                                    child: ListTile(
                                      dense: true,
                                      leading: Icon(choice.icon,
                                          color: Colors.grey[800]),
                                      title: Text(
                                        choice.title,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.grey[850],
                                        ),
                                      ),
                                    ),
                                  );
                                }).toList();
                              })
                        ],
                      ),
                    ),
                    Positioned(
                      left: 20.0,
                      bottom: 0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            classInfo.className,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 40.0,
                              fontWeight: FontWeight.w600,
                              letterSpacing: 1.2,
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                "${classInfo.classSection} (${classInfo.startTime} - ${classInfo.endTime})",
                                style: TextStyle(
                                  color: Colors.grey[700],
                                  fontSize: 25.0,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                _buildCardClockIn(context),
                _buildCardOptions(context)
              ]
            )
          )
        ],
      ),
    );
  }
}