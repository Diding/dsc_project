import 'dart:convert';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:dsc_project/app/view_models/crud_view_model.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';

class RecordsPageBuilder extends StatelessWidget {
  const RecordsPageBuilder({Key key, this.onRecordsPage}) : super(key: key);
  final VoidCallback onRecordsPage;

  static Future<void> showPage(BuildContext context) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.recordsPageBuilder,
        arguments: RecordsPageBuilderArguments(
          onRecordsPage: () => navigator.pop(),
        )
      );
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CRUDViewModel>(
      create: (_) => CRUDViewModel(),
      child: Consumer<CRUDViewModel>(
        builder: (_, CRUDViewModel model, __) => RecordsPage(
          model: model,
          onRecordsPage: onRecordsPage,
        ),
      ),
    );
  }
}

class RecordsPage extends StatefulWidget {
  const RecordsPage({Key key, @required this.model, this.onRecordsPage})
      : super(key: key);
  final CRUDViewModel model;
  final VoidCallback onRecordsPage;

  @override
  _RecordsPageState createState() => _RecordsPageState();
}

class _RecordsPageState extends State<RecordsPage> {
  CalendarController _controller;
  String pickedDate;

  @override
  void initState() {
    super.initState();
    _controller = CalendarController();
    pickedDate = '';
  }

  Widget _buildCalendar(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height / 1.7,
        child: Card(
            margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
            child: TableCalendar(
              onDaySelected: (date, events) {
                setState(() {
                  pickedDate = DateFormat('yyyy-MM-dd').format(date);
                });
              },
              calendarStyle: CalendarStyle(
                  selectedColor: Colors.teal, 
                  todayColor: Colors.grey[700]
                ),
              calendarController: _controller)
            )
          );
  }

  Widget _buildDetailsHolder(BuildContext context, attendanceDate) {
    String details;

    if (attendanceDate != null) {
      String displayDate = DateFormat('yyyy-MM-dd').format(attendanceDate);
      String displayTime = DateFormat('h:mm a').format(attendanceDate);
      details = 'Record Details: $displayDate at $displayTime';
    } else {
      details = 'No Attendance taken';
    }

    return Container(
        margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.black38,
        ),
        width: MediaQuery.of(context).size.width - 30,
        height: MediaQuery.of(context).size.width / 8,
        child: Center(
            child: Text(details,
                style: TextStyle(
                    color: Colors.white,
                    letterSpacing: 1.3,
                    fontFamily: 'Open Sans',
                    fontWeight: FontWeight.w500))));
  }

  Widget _buildCardRecordHolder(BuildContext context, onTap, IconData icon,
      String number, Color iconColor) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(90),
      ),
      color: Colors.transparent,
      child: Container(
          height: 90,
          width: 90,
          decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey[700],
                width: 4.0,
              ),
              color: Colors.grey[100],
              borderRadius: BorderRadius.circular(100)),
          child: InkWell(
            borderRadius: BorderRadius.circular(90),
            onTap: onTap,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Icon(icon, color: iconColor, size: 50),
                  Text(number,
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontFamily: 'Open Sans',
                          fontWeight: FontWeight.w600))
                ],
              ),
            ),
          )
        ),
    );
  }

  _buildStatusRecord(BuildContext context, List students, String status) {
    List<ListTile> listStudents = List<ListTile>();

    for (var i = 0; i < students.length; i++) {
      var studentRecord = jsonDecode(students[i]);
      var decodedStudentRecord = Student.fromJson(studentRecord);

      listStudents.add(ListTile(
        title: Text(
            '${decodedStudentRecord.lastname}, ${decodedStudentRecord.firstname}',
              style: TextStyle(fontFamily: 'Open Sans', fontWeight: FontWeight.w400),           
            ),
      ));
    }

    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              if (listStudents.length > 0) {
                return ListView.builder(
                      itemCount: listStudents.length,
                      itemBuilder: (context, index) {
                        return listStudents[index];
                      });
              } else {
                return Container(
                  child: Center(
                    child: Text('NO $status RECORDS', style: TextStyle(fontFamily: 'Open Sans', fontWeight: FontWeight.w400),),
                  ),
                );
              }
            }),
          );
        });
  }

  Widget _buildRecordsHolder(BuildContext context, presentRecord, absentRecord,
      lateRecord, presentStudents, absentStudents, lateStudents) {
    return Container(
        margin: EdgeInsets.fromLTRB(15, 5, 15, 15),
        padding: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.black26,
        ),
        width: MediaQuery.of(context).size.width - 30,
        height: MediaQuery.of(context).size.width / 3.0,
        child: Row(
          children: <Widget>[
            SizedBox(width: 12),
            _buildCardRecordHolder(
                context,
                () => _buildStatusRecord(context, presentStudents, 'PRESENT'),
                Icons.thumb_up,
                presentRecord.toString(),
                Colors.green),
            _buildCardRecordHolder(
                context,
                () => _buildStatusRecord(context, absentStudents, 'ABSENT'),
                Icons.thumb_down,
                absentRecord.toString(),
                Colors.red),
            _buildCardRecordHolder(
                context,
                () => _buildStatusRecord(context, lateStudents, 'LATE'),
                Icons.assignment_late,
                lateRecord.toString(),
                Colors.blueAccent)
          ],
        ));
  }

  Widget _buildRecordsUI(
      AsyncSnapshot<dynamic> snapshot, BuildContext context) {
    List presentStudents = [];
    List absentStudents = [];
    List lateStudents = [];
    int currentPresent = 0;
    int currentAbsent = 0;
    int currentLate = 0;
    int presentRecord = 0;
    int absentRecord = 0;
    int lateRecord = 0;
    DateTime attendanceDate;
    String formattedDate;
    List attendance;
    List record;
    var date;

    attendance = snapshot.data.documents
        .map((doc) => ClassAttendance.fromMap(doc.data, doc.documentID))
        .toList();

    for (var i = 0; i < attendance.length; i++) {
      var docs = attendance[i];
      var index;
      date = docs.dateChecked.toDate();
      formattedDate = DateFormat('yyyy-MM-dd').format(date);

      if (pickedDate == formattedDate) {
        index = i;
        attendanceDate = docs.dateChecked.toDate();
        record = jsonDecode(attendance[index].record);

        for (var i = 0; i < record.length; i++) {
          var individualRecord = jsonDecode(record[i]);
          var decodedRecord = Attendance.fromJson(individualRecord);

          if (decodedRecord.status == 'Present') {
            presentStudents.add(decodedRecord.student);
            currentPresent += 1;
          } else if (decodedRecord.status == 'Absent') {
            absentStudents.add(decodedRecord.student);
            currentAbsent += 1;
          } else if (decodedRecord.status == 'Late') {
            lateStudents.add(decodedRecord.student);
            currentLate += 1;
          }
        }
        presentRecord = currentPresent;
        absentRecord = currentAbsent;
        lateRecord = currentLate;
      }
    }

    if (attendance.length > 0) {
      return Column(children: <Widget>[
        _buildCalendar(context),
        _buildDetailsHolder(context, attendanceDate),
        _buildRecordsHolder(context, presentRecord, absentRecord, lateRecord,
            presentStudents, absentStudents, lateStudents),
      ]);
    } else {
      return Container(
        child: Center(
          child: Text("NO RECORDS YET"),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final attendanceProvider = Provider.of<CRUDViewModel>(context);
    final classId = appData.classId;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: Text('Records'),
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: StreamBuilder(
              stream: attendanceProvider.fetchCollectionAsStream(
                  'attendance', 'classId', classId),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return _buildRecordsUI(snapshot, context);
                } else {
                  return Column(children: <Widget>[
                    _buildCalendar(context),
                  ]);
                }
              }
            ),
        )
      );
  }
}