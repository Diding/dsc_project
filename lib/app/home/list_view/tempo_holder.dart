import 'dart:async';
import 'package:dsc_project/app/home/list_view/add_student_page.dart';
import 'package:dsc_project/app/view_models/crud_view_model.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dsc_project/services/firebase_auth_service.dart';
import 'package:dsc_project/common_widgets/form_submit_button.dart';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:flutter/foundation.dart';

class TempoHolderPageBuilder extends StatelessWidget {
  const TempoHolderPageBuilder({Key key, this.onTempoHolderpage}) : super(key: key);
  final VoidCallback onTempoHolderpage;

  static Future<void> showPage(BuildContext context) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.tempoHolderPageBuilder,
        arguments: TempoHolderPageBuilderArguments(
          onTempoHolderpage: () => navigator.pop(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CRUDViewModel>(
      create: (_) => CRUDViewModel(),
      child: Consumer<CRUDViewModel>(
        builder: (_, CRUDViewModel model, __) => TempoHolder(
          model: model, onTempoHolderpage: onTempoHolderpage),
      ),
    );
  }
}

class TempoHolder extends StatefulWidget {
  const TempoHolder({Key key, @required this.model, this.onTempoHolderpage})
      : super(key: key);
  final CRUDViewModel model;
  final VoidCallback onTempoHolderpage;

  @override
  _TempoHolderState createState() => _TempoHolderState();
}

class _TempoHolderState extends State<TempoHolder> {
  final databaseReference = Firestore.instance;
  final formKey = GlobalKey<FormState>();
  var selectedClass;
  
  Widget _buildSubmitButton() {
    return FormSubmitButton(
      text: 'Select',
      onPressed: () {
        AddStudentPageBuilder.showPage(context);
      },
    );
  }

  Widget _buildDropdownMenu(AsyncSnapshot<dynamic> snapshot) {
    List classes;
    List<DropdownMenuItem> classItems = [];
    classes = snapshot.data.documents
        .map((doc) => Class.fromMap(doc.data, doc.documentID))
        .toList();

    for (int i = 0; i < classes.length; i++) {
      var docs = classes[i];
      classItems.add(DropdownMenuItem(
        child: Text('${docs.className} ${docs.classSection}',
            style: TextStyle(color: Colors.teal[700])),
        value: "${docs.id}",
      ));
    }
    if (classItems.length > 0) {
      return Container(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 10),
              SizedBox(height: 70),
              Text("SELECT YOUR CLASS",
                  style: TextStyle(fontSize: 15, color: Colors.grey[700])),
              SizedBox(height: 10),
              DropdownButton(
                items: classItems,
                onChanged: (classValue) {
                  setState(() {
                    selectedClass = classValue;
                  });
                  appData.classId = selectedClass;
                },
                value: selectedClass,
                isExpanded: false,
              ),
              SizedBox(height: 60),
              _buildSubmitButton(),
            ],
          ),
        ),
      );
    } else {
      return Container(
        child: Center(
          child: Text('NO CLASSES YET'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final classProvider = Provider.of<CRUDViewModel>(context);
    final user = Provider.of<User>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text('Add Student'),
        ),
        resizeToAvoidBottomPadding: false,
        body: Container(
          child: StreamBuilder(
              stream: classProvider.fetchCollectionAsStream(
                  'classes', 'uid', user.uid),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return _buildDropdownMenu(snapshot);
                } else {
                  return Text('');
                }
              }
            ),
        )
      );
  }
}