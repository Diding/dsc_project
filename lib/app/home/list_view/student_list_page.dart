import 'package:dsc_project/app/view_models/crud_view_model.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StudentListPageBuilder extends StatelessWidget {
  const StudentListPageBuilder({Key key, this.onStudentListPage})
      : super(key: key);
  final VoidCallback onStudentListPage;

  static Future<void> showPage(BuildContext context) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.studentListPageBuilder,
        arguments: StudentListPageBuilderArguments(
          onStudentListPage: () => navigator.pop(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CRUDViewModel>(
      create: (_) => CRUDViewModel(),
      child: Consumer<CRUDViewModel>(
        builder: (_, CRUDViewModel model, __) =>
            StudentListPage(model: model, onStudentListPage: onStudentListPage),
      ),
    );
  }
}

class StudentListPage extends StatefulWidget {
  const StudentListPage({Key key, @required this.model, this.onStudentListPage})
      : super(key: key);
  final CRUDViewModel model;
  final VoidCallback onStudentListPage;

  @override
  _StudentListPageState createState() => _StudentListPageState();
}

class _StudentListPageState extends State<StudentListPage> {

  @override
  void initState() {
    super.initState();
    widget.model.getClassData(appData.classId).then((mod) {
      setState(() {
        widget.model.setData();
      });
    });
  }

  Widget _editButton() {
    return IconButton(icon: Icon(Icons.edit), onPressed: null);
  }

  Widget _buildTile(AsyncSnapshot<dynamic> snapshot, BuildContext context) {
    List students;
    List<ListTile> list = List<ListTile>();

    students = snapshot.data.documents
        .map((doc) => Student.fromMap(doc.data, doc.documentID))
        .toList();
        
    for (var i = 0; i < students.length; i++) {
      var docs = students[i];
      list.add(ListTile(
        title: Text('${docs.lastname}, ${docs.firstname}'),
        subtitle: Text('${docs.parentContactNumber}'),
        trailing: _editButton(),
      ));
    }

    if (list.length > 0) {
      return ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          return list[index];
        },
      );
    } else {
      return Container(
        child: Center(
          child: Text("NO STUDENTS YET"),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final studentProvider = Provider.of<CRUDViewModel>(context);
    final classId = appData.classId;

    return Scaffold(
      appBar: AppBar(
        title: Text('Student Lists', style: TextStyle(fontSize: 18)),
      ),
      body: Container(
          child: StreamBuilder(
              stream: studentProvider.fetchCollectionAsStream(
                  'students', 'classId', classId),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return _buildTile(snapshot, context);
                } else {
                  return Container(
                    child: Center(
                      child: Text("NO STUDENTS YET"),
                    ),
                  );
                }
              }
            )
          ),
    );
  }
}