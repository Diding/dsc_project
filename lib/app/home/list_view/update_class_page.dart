import 'package:dsc_project/app/home/home_page.dart';
import 'package:dsc_project/app/home/list_view/update_class_view_model.dart';
import 'package:dsc_project/app/home/list_view/validator.dart';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:dsc_project/common_widgets/custom_tile.dart';
import 'package:dsc_project/common_widgets/form_submit_button.dart';
import 'package:dsc_project/common_widgets/pop_up_message.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:dsc_project/services/firebase_auth_service.dart';
import 'package:dsc_project/services/class_services.dart';
import 'package:flutter/material.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:provider/provider.dart';
import 'package:dsc_project/app/view_models/crud_view_model.dart';

class UpdateClassPageBuilder extends StatelessWidget {
  final Class classInfo;
  const UpdateClassPageBuilder({Key key, this.onViewClassPage, this.classInfo})
      : super(key: key);
  final VoidCallback onViewClassPage;

  static Future<void> showPage(BuildContext context, Class classInfo) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.updateClassPageBuilder,
        arguments: UpdateClassPageBuilderArguments(
            onViewClassPage: () => navigator.pop(), classInfo: classInfo));
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<UpdateClassPageViewModel>(
      create: (_) => UpdateClassPageViewModel(),
      child: Consumer<UpdateClassPageViewModel>(
        builder: (_, UpdateClassPageViewModel model, __) => UpdateClassPage(
          model: model,
          onViewClassPage: onViewClassPage,
          classInfo: classInfo,
        ),
      ),
    );
  }
}

class UpdateClassPage extends StatefulWidget {
  const UpdateClassPage(
      {Key key, @required this.model, this.onViewClassPage, this.classInfo})
      : super(key: key);
  final UpdateClassPageViewModel model;
  final VoidCallback onViewClassPage;
  final Class classInfo;

  @override
  _UpdateClassPageState createState() => _UpdateClassPageState();
}

class _UpdateClassPageState extends State<UpdateClassPage> {
  Class classInfo;
  _UpdateClassPageState({this.classInfo});
  ClassServices newClassService = ClassServices();
  CRUDViewModel classProvider = CRUDViewModel();
  final formKey = GlobalKey<FormState>();
  String classSection;
  String className;
  TimeOfDay currentTime = TimeOfDay.now();
  TimeOfDay timePicked;
  String end = '';
  String start = '';

  Future<void> startTime(BuildContext context) async {
    timePicked = await showTimePicker(
      context: context,
      initialTime: currentTime,
    );

    MaterialLocalizations localizations = MaterialLocalizations.of(context);
    String formattedTime =
        localizations.formatTimeOfDay(timePicked, alwaysUse24HourFormat: true);
    setState(() {
      start = formattedTime;
      appData.startTime = start;
    });
  }

  Future<void> endTime(BuildContext context) async {
    timePicked = await showTimePicker(
      context: context,
      initialTime: currentTime,
    );

    MaterialLocalizations localizations = MaterialLocalizations.of(context);
    String formattedTime =
        localizations.formatTimeOfDay(timePicked, alwaysUse24HourFormat: true);
    setState(() {
      end = formattedTime;
      appData.endTime = end;
    });
  }

  Widget _inputField(String title, String initialValue) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 3),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            initialValue: initialValue,
            style: TextStyle(
                color: Colors.teal[700],
                fontFamily: 'Open Sans',
                fontSize: 18.0),
            onChanged: (value) {
              if (title == 'Class Section') {
                classSection = value;
                appData.classSection = classSection;
              } else {
                className = value;
                appData.className = className;
              }
            },
            validator: (value) {
             if (title == 'Class Section') {
                return FieldValidator.validateClassSection(value);
              } else {
                return FieldValidator.validateClassName(value);
              }
            },
            decoration: InputDecoration(
              labelText: title,
              labelStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                  fontFamily: 'Open Sans',
                  fontSize: 18.0),
            ),
          ),
        ],
      ),
    );
  }

  Widget _inputWidget() {
    return Container(
      margin: EdgeInsets.all(0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 10.0),
          _inputField('Class Section', '${appData.classSection}'),
          _inputField('Subject Name', '${appData.className}'),
          SizedBox(height: 10.0),
        ],
      ),
    );
  }

  Widget _listTile(BuildContext context) {
    return Column(
      children: <Widget>[
        CustomListTile(
          icon: Icons.access_time,
          text: 'Start Time',
          onTap: () {
            startTime(context);
          },
          label: '${appData.startTime}',
        ),
        CustomListTile(
          icon: Icons.access_time,
          text: 'End Time',
          onTap: () {
            endTime(context);
          },
          label: '${appData.endTime}',
        ),
      ],
    );
  }


  Widget _submitButton() {
    return FormSubmitButton(
        text: 'Update Class',
        onPressed: () async {
          if (formKey.currentState.validate()) {
            formKey.currentState.save();

            try {
              await classProvider.updateClass(
                  appData.className,
                  appData.classSection,
                  appData.startTime,
                  appData.endTime,
                  appData.classId);
              Future.delayed(
                Duration(seconds: 2),
                () {
                  PopUpMessage().popUpMessage(
                      'Class SUCCESSFULLY updated',
                      'Class Section: ${appData.classSection} \n Class Name: ${appData.className}',
                      () => Navigator.push(context,
                          MaterialPageRoute(builder: (context) => HomePage())),
                      context);
                },
              );
            } catch (e) {
              Future.delayed(
                Duration(seconds: 2),
                () {
                  PopUpMessage().popUpMessage(
                      'Sorry! an ERROR occurred',
                      'Error detail: ${e.toString()}',
                      () => Navigator.of(context).pop(),
                      context);
                },
              );
            }
          }
        }
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Update Class"),
      ),
      resizeToAvoidBottomPadding: false,
      body: Form(
        key: formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              color: Colors.white,
              height: MediaQuery.of(context).size.height - 80,
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 20.0),
                  _inputWidget(),
                  _listTile(context),
                  SizedBox(height: 50.0),
                  _submitButton(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}