import 'package:dsc_project/app/home/list_view/add_student_view_model.dart';
import 'package:dsc_project/app/home/list_view/student_list_page.dart';
import 'package:dsc_project/app/home/list_view/students_page.dart';
import 'package:dsc_project/app/home/list_view/validator.dart';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:dsc_project/common_widgets/form_submit_button.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:dsc_project/services/student_record_services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dsc_project/services/student_services.dart';
import 'package:dsc_project/services/firebase_auth_service.dart';
import 'package:dsc_project/common_widgets/pop_up_message.dart';

class AddStudentPageBuilder extends StatelessWidget {
  const AddStudentPageBuilder({Key key, this.onStudentPage}) : super(key: key);
  final VoidCallback onStudentPage;

  static Future<void> showPage(BuildContext context) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.addStudentPageBuilder,
        arguments: AddStudentPageBuilderArguments(
          onStudentPage: () => navigator.pop(),
        ));
  }

  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AddStudentPageViewModel>(
      create: (_) => AddStudentPageViewModel(),
      child: Consumer<AddStudentPageViewModel>(
        builder: (_, AddStudentPageViewModel model, __) =>
            AddStudentPage(model: model, onStudentPage: onStudentPage),
      ),
    );
  }
}

class AddStudentPage extends StatefulWidget {
  const AddStudentPage({Key key, @required this.model, this.onStudentPage})
      : super(key: key);
  final AddStudentPageViewModel model;
  final VoidCallback onStudentPage;

  @override
  _AddStudentPageState createState() => _AddStudentPageState();
}

class _AddStudentPageState extends State<AddStudentPage> {
  StudentServices addStudentService = StudentServices();
  StudentRecordServices newStudentRecordService = StudentRecordServices();
  final databaseReference = Firestore.instance;
  final formKey = GlobalKey<FormState>();
  String firstname;
  String lastname;
  String parentContactNumber;

  Widget _inputField(String title, int numKey) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 3),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            key: ValueKey<int>(numKey),
            style: TextStyle(
                color: Colors.teal[700],
                fontFamily: 'Open Sans',
                fontSize: 18.0),
            onChanged: (value) {
              if (title == 'First Name') firstname = value;
              if (title == 'Last Name') lastname = value;
              if (title == 'Parent Contact Number') parentContactNumber = value;
            },
            validator: (value) {
              if (title == 'First Name') {
                return FieldValidator.validateFirstName(value);
              } else if (title == 'Last Name') {
                return FieldValidator.validateLastName(value);
              } else {
                return FieldValidator.validPhoneNumber(value);
              }
            },
            decoration: InputDecoration(
              labelText: title,
              labelStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                  fontFamily: 'Open Sans',
                  fontSize: 18.0),
            ),
          ),
        ],
      ),
    );
  }

  Widget _inputWidget() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 50, 0, 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _inputField('First Name', 1),
          _inputField('Last Name', 2),
          _inputField('Parent Contact Number', 3)
        ],
      ),
    );
  }

  Widget _submitButton() {
    final user = Provider.of<User>(context);
    String classId = appData.classId;

    return FormSubmitButton(
        text: 'Add Student',
        onPressed: () async {
          if (formKey.currentState.validate()) {
            formKey.currentState.save();

            try {
              String studentId = await addStudentService.addNewStudent(
                  classId, firstname, lastname, parentContactNumber);

              await newStudentRecordService.submitStudentRecord(
                  studentId, 0, 0, 0);

              Future.delayed(
                Duration(seconds: 2),
                () {
                  PopUpMessage().popUpMessage(
                      'Student successfully added',
                      'Name: $firstname $lastname',
                      () => StudentsPageBuilder.showPage(context),
                      context
                    );
                },
              );
            } catch (e) {
              Future.delayed(
                Duration(seconds: 2),
                () {
                  PopUpMessage().popUpMessage(
                      'Sorry! an ERROR occurred',
                      'Error detail: ${e.toString()}',
                      () => Navigator.of(context).pop(),
                      context);
                },
              );
            }
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("New Student"),
      ),
      resizeToAvoidBottomPadding: false,
      body: Form(
        key: formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              color: Colors.white,
              height: MediaQuery.of(context).size.height - 80,
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  _inputWidget(),
                  SizedBox(height: 50.0),
                  _submitButton(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
