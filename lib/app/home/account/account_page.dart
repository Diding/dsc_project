import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dsc_project/common_widgets/form_submit_button.dart';
import 'package:flutter/material.dart';
import 'package:dsc_project/routing/router.gr.dart';
import 'package:provider/provider.dart';
import 'account_view_model.dart';
import 'package:dsc_project/services/firebase_auth_service.dart';
import 'package:dsc_project/services/edit_account_service.dart';
import 'package:dsc_project/common_widgets/pop_up_message.dart';

class AccountPageBuilder extends StatelessWidget {
  const AccountPageBuilder({Key key, this.onViewAccountPage})
      : super(key: key);
  final VoidCallback onViewAccountPage;

  static Future<void> showPage(BuildContext context) async {
    final navigator = Navigator.of(context);
    await navigator.pushNamed(Router.accountPageBuilder,
        arguments: AccountPageBuilderArguments(
          onViewAccountPage: () => navigator.pop(),
        )
      );
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AccountPageViewModel>(
      create: (_) => AccountPageViewModel(),
      child: Consumer<AccountPageViewModel>(
        builder: (_, AccountPageViewModel model, __) => AccountPage(
            model: model, onViewAccountPage: onViewAccountPage),
      ),
    );
  }
}

class AccountPage extends StatefulWidget {
   const AccountPage({Key key, @required this.model, this.onViewAccountPage})
      : super(key: key);
  final AccountPageViewModel model;
  final VoidCallback onViewAccountPage;

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  final databaseReference = Firestore.instance;
  final formKey = GlobalKey<FormState>();
  String displayName;
  String photoUrl;

  Widget _inputField(String title) {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 3),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        TextFormField(
          style: TextStyle(
            color: Colors.teal[700],
            fontFamily: 'Open Sans',
            fontSize: 18.0
          ),
          onChanged: (value) {
            if (title == 'Display Name') displayName = value;
            if (title == 'Photo Url') photoUrl = value;
          },
          validator: (value) {
            return value.isEmpty ? '$title must not be blank' : null;
          },
          decoration: InputDecoration(
            labelText: title,
            labelStyle: 
              TextStyle(fontWeight: FontWeight.bold, color: Colors.black87, fontFamily: 'Open Sans', fontSize: 18.0),
            ),
          ),
        ],
      ),
    );
  }

  Widget _inputWidget() {
    return Container(
      margin: EdgeInsets.all(0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
            SizedBox(height: 10.0),
          _inputField('Display Name'),
          _inputField('Photo Url'),
            SizedBox(height: 10.0),
        ],
      ),
    );
  }

  Widget _submitButton(User user) {
    return FormSubmitButton(
      text: 'Edit Profile',
      onPressed: () async {
        if (formKey.currentState.validate()) {
          formKey.currentState.save();

            try {
              await AccountDatabase(uid: user.uid).editUser(displayName, photoUrl);
              Future.delayed(
                Duration(seconds: 1),
                () {
                  PopUpMessage().popUpMessage(
                      'Profile successfully edited',
                      '',
                      () => 
                      Navigator.of(context).pop(),
                      context);
                  },
              );
            } catch (e) {
              Future.delayed(
                Duration(seconds: 1),
                () {
                  PopUpMessage().popUpMessage('Sorry! an ERROR occurred',
                      'Error detail: ${e.toString()}', 
                      () => 
                      Navigator.of(context).pop(),
                      context);
                },
              );
            }
        }
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Profile"),
      ),
      resizeToAvoidBottomPadding: false,
      body: Form(
        key: formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
               color: Colors.white,
              height: MediaQuery.of(context).size.height - 80,
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 20.0),
                  _inputWidget(),
                  SizedBox(height: 50.0),
                  _submitButton(user),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}