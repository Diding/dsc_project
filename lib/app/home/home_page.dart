import 'dart:async';
import 'package:dsc_project/app/home/list_view/add_class_page.dart';
import 'package:dsc_project/app/home/list_view/attendance_check_page.dart';
import 'package:dsc_project/app/home/list_view/class_page.dart';
import 'package:dsc_project/app/home/list_view/tempo_holder.dart';
import 'package:dsc_project/app/singletons/app_data.dart';
import 'package:dsc_project/common_widgets/custom_tile.dart';
import 'package:dsc_project/common_widgets/platform_alert_dialog.dart';
import 'package:dsc_project/common_widgets/platform_exception_alert_dialog.dart';
import 'package:dsc_project/constants/strings.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:dsc_project/app/view_models/crud_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:dsc_project/services/firebase_auth_service.dart';
import 'package:dsc_project/app/home/account/account_page.dart';

class HomePage extends StatelessWidget {
  final CRUDViewModel classProvider = CRUDViewModel();

  Future<void> _signOut(BuildContext context) async {
    try {
      final FirebaseAuthService auth =
          Provider.of<FirebaseAuthService>(context, listen: false);
      await auth.signOut();
    } on PlatformException catch (e) {
      await PlatformExceptionAlertDialog(
        title: Strings.logoutFailed,
        exception: e,
      ).show(context);
    }
  }

  Future<void> _confirmSignOut(BuildContext context) async {
    final bool didRequestSignOut = await PlatformAlertDialog(
      title: Strings.logout,
      content: Strings.logoutAreYouSure,
      cancelActionText: Strings.cancel,
      defaultActionText: Strings.logout,
    ).show(context);
    if (didRequestSignOut == true) {
      _signOut(context);
    }
  }

  Widget _buildUserInfo(User user, BuildContext context) {
    return Container(
      color: Colors.white,
      child: UserAccountsDrawerHeader(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        accountName:
            Text(user.displayName, style: TextStyle(color: Colors.teal)),
        accountEmail:
            Text(user.email, style: 
              TextStyle(color: Colors.grey[700], 
                fontSize: 16, letterSpacing: 0.7, 
                fontWeight: FontWeight.w400, 
                fontStyle: FontStyle.italic,
              )),
        currentAccountPicture: CircleAvatar(
          child: Icon(Icons.account_circle, size: 80),
        ),
        onDetailsPressed: () => AccountPageBuilder.showPage(context),
      ),
    );
  }

  Widget _buildListViewLabels(BuildContext context) {
    return Column(
      children: <Widget>[
        CustomListTile(
          icon: Icons.calendar_today,
          text: Strings.addClass,
          arrowIcon: Icons.arrow_right,
          onTap: () => AddClassPageBuilder.showPage(context),
          label: '',
        ),
        CustomListTile(
          icon: Icons.account_box,
          text: Strings.addStudent,
          arrowIcon: Icons.arrow_right,
          onTap: () => TempoHolderPageBuilder.showPage(context),
          label: '',
        ),
        CustomListTile(
          icon: Icons.check_box,
          text: Strings.attendance,
          arrowIcon: Icons.arrow_right,
          onTap: () => AttendanceCheckPageBuilder.showPage(context),
          label: '',
        ),
        CustomListTile(
          icon: Icons.help,
          text: Strings.help,
          arrowIcon: Icons.arrow_right,
          onTap: () {},
          label: '',
        ),
        CustomListTile(
          icon: Icons.search,
          text: Strings.about,
          arrowIcon: Icons.arrow_right,
          onTap: () {},
          label: '',
        ),
        CustomListTile(
          icon: Icons.person,
          text: Strings.logout,
          onTap: () => _confirmSignOut(context),
          label: '',
        ),
      ],
    );
  }

  Widget _classCard(classes, i, context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(3.0),
      ),
      color: Colors.black,
      child: Container(
          height: MediaQuery.of(context).size.height / 7.5,
          child: InkWell(
            onTap: () {
              appData.classId = classes[i].id;
              ClassPageBuilder.showPage(context, classes[i]);
            },
            splashColor: Colors.white,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(classes[i].className,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic,
                          fontFamily: 'Open Sans',
                          fontSize: 20)),
                  Text(classes[i].classSection,
                      style: TextStyle(color: Colors.white)),
                  Text(classes[i].startTime,
                      style: TextStyle(color: Colors.white)),
                  Text(classes[i].endTime,
                      style: TextStyle(color: Colors.white)),
                ],
              ),
            ),
          )),
    );
  }

  Widget _currentClassCard(classInfo, context) {
    var center = MediaQuery.of(context).size.width / 2;
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(3.0),
        ),
        color: Colors.teal[200],
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(center - 50, 0, 0, 0),
              height: MediaQuery.of(context).size.height / 6.5,
              child: InkWell(
                onTap: () {
                  appData.classId = classInfo.id;
                  ClassPageBuilder.showPage(context, classInfo);
                },
                splashColor: Colors.teal,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(classInfo.className,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                              fontFamily: 'Open Sans',
                              fontSize: 20)),
                      Text(classInfo.classSection),
                      Text(classInfo.startTime),
                      Text(classInfo.endTime),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  Widget _buildDashboard(
      AsyncSnapshot<dynamic> snapshot, BuildContext context) {
    List<Card> list = List<Card>();
    TimeOfDay currentTime = TimeOfDay.now();
    Card currentClass;
    String timeNow;
    List classes;

    MaterialLocalizations localizations = MaterialLocalizations.of(context);
    String formattedTime =
        localizations.formatTimeOfDay(currentTime, alwaysUse24HourFormat: true);
    timeNow = formattedTime;

    _convertTimeToInt(String time) {
      return int.parse(time.split(":")[0] + time.split(":")[0]);
    }

    classes = snapshot.data.documents
        .map((doc) => Class.fromMap(doc.data, doc.documentID))
        .toList();
    for (var i = 0; i < classes.length; i++) {
      int now = _convertTimeToInt(timeNow);
      int start = _convertTimeToInt(classes[i].startTime);
      int end = _convertTimeToInt(classes[i].endTime);

      if (now >= start && now <= end) {
        currentClass = _currentClassCard(classes[i], context);
      }
      list.add(_classCard(classes, i, context));
    }

    if (list.length > 0) {
      return CustomScrollView(
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: Container(
              padding: const EdgeInsets.all(8.0),
              child: Text("Next/Current Class",
                  style: TextStyle(
                      color: Colors.grey[700],
                      fontWeight: FontWeight.bold,
                      fontSize: 15)),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: const EdgeInsets.all(8.0),
              child: currentClass,
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: const EdgeInsets.all(8.0),
              child: Text("All Classes",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[700],
                      fontSize: 15)),
            ),
          ),
          SliverGrid(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return Container(
                  alignment: Alignment.center,
                  child: list[index],
                );
              },
              childCount: list.length,
            ),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 15,
              crossAxisSpacing: 15,
              childAspectRatio: 1.5,
            ),
          ),
        ],
      );
    } else {
      return Container(
        child: Center(
          child: Text('NO CLASSES YET'),
        ),
      );
    }
  }

  _buildDrawer(User user, BuildContext context) {
    return Container(
      width: 243,
      child: Drawer(
        elevation: 10,
        child: ListView(
          children: <Widget>[
            _buildUserInfo(user, context),
            Container(
              padding: EdgeInsets.all(20.0),
              height: MediaQuery.of(context).size.height - 190,
              decoration: BoxDecoration(color: Colors.white),
              child: Column(children: <Widget>[
                _buildListViewLabels(context),
              ]),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    CRUDViewModel classProvider = CRUDViewModel();
    final user = Provider.of<User>(context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(Strings.dashboard, style: TextStyle(color: Colors.black)),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      drawer: _buildDrawer(user, context),
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/bg.png'), fit: BoxFit.cover),
          ),
          padding: EdgeInsets.all(15),
          child: StreamBuilder(
            stream: classProvider.fetchCollectionAsStream(
                'classes', 'uid', user.uid),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return _buildDashboard(snapshot, context);
              } else {
                return Text('');
              }
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add, color: Colors.black),
          backgroundColor: Colors.white,
          onPressed: () {
            AddClassPageBuilder.showPage(context);
          }),
    );
  }
}
