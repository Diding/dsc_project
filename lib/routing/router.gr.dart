// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/router_utils.dart';
import 'package:dsc_project/app/auth_widget.dart';
import 'package:dsc_project/services/firebase_auth_service.dart';
import 'package:dsc_project/app/sign_in/email_password/email_password_sign_in_page.dart';
import 'package:dsc_project/app/home/list_view/attendance_check_page.dart';
import 'package:dsc_project/app/home/list_view/add_class_page.dart';
import 'package:dsc_project/app/home/list_view/manual_checklist_page.dart';
import 'package:dsc_project/app/home/list_view/class_page.dart';
import 'package:dsc_project/services/class_models.dart';
import 'package:dsc_project/app/home/list_view/add_student_page.dart';
import 'package:dsc_project/app/home/account/account_page.dart';
import 'package:dsc_project/app/home/list_view/student_list_page.dart';
import 'package:dsc_project/app/home/list_view/tempo_holder.dart';
import 'package:dsc_project/app/home/list_view/update_class_page.dart';
import 'package:dsc_project/app/home/list_view/records_page.dart';
import 'package:dsc_project/app/home/list_view/student_statistics_page.dart';
import 'package:dsc_project/app/home/list_view/students_page.dart';

class Router {
  static const authWidget = '/';
  static const emailPasswordSignInPageBuilder =
      '/email-password-sign-in-page-builder';
  static const attendanceCheckPageBuilder = '/attendance-check-page-builder';
  static const addClassPageBuilder = '/add-class-page-builder';
  static const manualChecklistPageBuilder = '/manual-checklist-page-builder';
  static const classPageBuilder = '/class-page-builder';
  static const addStudentPageBuilder = '/add-student-page-builder';
  static const accountPageBuilder = '/account-page-builder';
  static const studentListPageBuilder = '/student-list-page-builder';
  static const tempoHolderPageBuilder = '/tempo-holder-page-builder';
  static const updateClassPageBuilder = '/update-class-page-builder';
  static const recordsPageBuilder = '/records-page-builder';
  static const studentStatisticsPageBuilder =
      '/student-statistics-page-builder';
  static const studentsPageBuilder = '/students-page-builder';
  static GlobalKey<NavigatorState> get navigatorKey =>
      getNavigatorKey<Router>();
  static NavigatorState get navigator => navigatorKey.currentState;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Router.authWidget:
        if (hasInvalidArgs<AuthWidgetArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<AuthWidgetArguments>(args);
        }
        final typedArgs = args as AuthWidgetArguments;
        return MaterialPageRoute(
          builder: (_) => AuthWidget(
              key: typedArgs.key, userSnapshot: typedArgs.userSnapshot),
          settings: settings,
        );
      case Router.emailPasswordSignInPageBuilder:
        if (hasInvalidArgs<EmailPasswordSignInPageBuilderArguments>(args)) {
          return misTypedArgsRoute<EmailPasswordSignInPageBuilderArguments>(
              args);
        }
        final typedArgs = args as EmailPasswordSignInPageBuilderArguments ??
            EmailPasswordSignInPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => EmailPasswordSignInPageBuilder(
              key: typedArgs.key, onSignedIn: typedArgs.onSignedIn),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.attendanceCheckPageBuilder:
        if (hasInvalidArgs<AttendanceCheckPageBuilderArguments>(args)) {
          return misTypedArgsRoute<AttendanceCheckPageBuilderArguments>(args);
        }
        final typedArgs = args as AttendanceCheckPageBuilderArguments ??
            AttendanceCheckPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => AttendanceCheckPageBuilder(
              key: typedArgs.key, onAttendancePage: typedArgs.onAttendancePage),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.addClassPageBuilder:
        if (hasInvalidArgs<AddClassPageBuilderArguments>(args)) {
          return misTypedArgsRoute<AddClassPageBuilderArguments>(args);
        }
        final typedArgs = args as AddClassPageBuilderArguments ??
            AddClassPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => AddClassPageBuilder(
              key: typedArgs.key, onClassPage: typedArgs.onClassPage),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.manualChecklistPageBuilder:
        if (hasInvalidArgs<ManualChecklistPageBuilderArguments>(args)) {
          return misTypedArgsRoute<ManualChecklistPageBuilderArguments>(args);
        }
        final typedArgs = args as ManualChecklistPageBuilderArguments ??
            ManualChecklistPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => ManualChecklistPageBuilder(
              key: typedArgs.key, onChecklistPage: typedArgs.onChecklistPage),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.classPageBuilder:
        if (hasInvalidArgs<ClassPageBuilderArguments>(args)) {
          return misTypedArgsRoute<ClassPageBuilderArguments>(args);
        }
        final typedArgs =
            args as ClassPageBuilderArguments ?? ClassPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => ClassPageBuilder(
              key: typedArgs.key,
              onViewClassPage: typedArgs.onViewClassPage,
              classInfo: typedArgs.classInfo),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.addStudentPageBuilder:
        if (hasInvalidArgs<AddStudentPageBuilderArguments>(args)) {
          return misTypedArgsRoute<AddStudentPageBuilderArguments>(args);
        }
        final typedArgs = args as AddStudentPageBuilderArguments ??
            AddStudentPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => AddStudentPageBuilder(
              key: typedArgs.key, onStudentPage: typedArgs.onStudentPage),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.accountPageBuilder:
        if (hasInvalidArgs<AccountPageBuilderArguments>(args)) {
          return misTypedArgsRoute<AccountPageBuilderArguments>(args);
        }
        final typedArgs = args as AccountPageBuilderArguments ??
            AccountPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => AccountPageBuilder(
              key: typedArgs.key,
              onViewAccountPage: typedArgs.onViewAccountPage),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.studentListPageBuilder:
        if (hasInvalidArgs<StudentListPageBuilderArguments>(args)) {
          return misTypedArgsRoute<StudentListPageBuilderArguments>(args);
        }
        final typedArgs = args as StudentListPageBuilderArguments ??
            StudentListPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => StudentListPageBuilder(
              key: typedArgs.key,
              onStudentListPage: typedArgs.onStudentListPage),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.tempoHolderPageBuilder:
        if (hasInvalidArgs<TempoHolderPageBuilderArguments>(args)) {
          return misTypedArgsRoute<TempoHolderPageBuilderArguments>(args);
        }
        final typedArgs = args as TempoHolderPageBuilderArguments ??
            TempoHolderPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => TempoHolderPageBuilder(
              key: typedArgs.key,
              onTempoHolderpage: typedArgs.onTempoHolderpage),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.updateClassPageBuilder:
        if (hasInvalidArgs<UpdateClassPageBuilderArguments>(args)) {
          return misTypedArgsRoute<UpdateClassPageBuilderArguments>(args);
        }
        final typedArgs = args as UpdateClassPageBuilderArguments ??
            UpdateClassPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => UpdateClassPageBuilder(
              key: typedArgs.key,
              onViewClassPage: typedArgs.onViewClassPage,
              classInfo: typedArgs.classInfo),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.recordsPageBuilder:
        if (hasInvalidArgs<RecordsPageBuilderArguments>(args)) {
          return misTypedArgsRoute<RecordsPageBuilderArguments>(args);
        }
        final typedArgs = args as RecordsPageBuilderArguments ??
            RecordsPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => RecordsPageBuilder(
              key: typedArgs.key, onRecordsPage: typedArgs.onRecordsPage),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.studentStatisticsPageBuilder:
        if (hasInvalidArgs<StudentStatisticsPageBuilderArguments>(args)) {
          return misTypedArgsRoute<StudentStatisticsPageBuilderArguments>(args);
        }
        final typedArgs = args as StudentStatisticsPageBuilderArguments ??
            StudentStatisticsPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => StudentStatisticsPageBuilder(
              key: typedArgs.key,
              onStudentStatisticsPage: typedArgs.onStudentStatisticsPage),
          settings: settings,
          fullscreenDialog: true,
        );
      case Router.studentsPageBuilder:
        if (hasInvalidArgs<StudentsPageBuilderArguments>(args)) {
          return misTypedArgsRoute<StudentsPageBuilderArguments>(args);
        }
        final typedArgs = args as StudentsPageBuilderArguments ??
            StudentsPageBuilderArguments();
        return MaterialPageRoute(
          builder: (_) => StudentsPageBuilder(
              key: typedArgs.key, onStudentsPage: typedArgs.onStudentsPage),
          settings: settings,
          fullscreenDialog: true,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//**************************************************************************
// Arguments holder classes
//***************************************************************************

//AuthWidget arguments holder class
class AuthWidgetArguments {
  final Key key;
  final AsyncSnapshot<User> userSnapshot;
  AuthWidgetArguments({this.key, @required this.userSnapshot});
}

//EmailPasswordSignInPageBuilder arguments holder class
class EmailPasswordSignInPageBuilderArguments {
  final Key key;
  final void Function() onSignedIn;
  EmailPasswordSignInPageBuilderArguments({this.key, this.onSignedIn});
}

//AttendanceCheckPageBuilder arguments holder class
class AttendanceCheckPageBuilderArguments {
  final Key key;
  final void Function() onAttendancePage;
  AttendanceCheckPageBuilderArguments({this.key, this.onAttendancePage});
}

//AddClassPageBuilder arguments holder class
class AddClassPageBuilderArguments {
  final Key key;
  final void Function() onClassPage;
  AddClassPageBuilderArguments({this.key, this.onClassPage});
}

//ManualChecklistPageBuilder arguments holder class
class ManualChecklistPageBuilderArguments {
  final Key key;
  final void Function() onChecklistPage;
  ManualChecklistPageBuilderArguments({this.key, this.onChecklistPage});
}

//ClassPageBuilder arguments holder class
class ClassPageBuilderArguments {
  final Key key;
  final void Function() onViewClassPage;
  final Class classInfo;
  ClassPageBuilderArguments({this.key, this.onViewClassPage, this.classInfo});
}

//AddStudentPageBuilder arguments holder class
class AddStudentPageBuilderArguments {
  final Key key;
  final void Function() onStudentPage;
  AddStudentPageBuilderArguments({this.key, this.onStudentPage});
}

//AccountPageBuilder arguments holder class
class AccountPageBuilderArguments {
  final Key key;
  final void Function() onViewAccountPage;
  AccountPageBuilderArguments({this.key, this.onViewAccountPage});
}

//StudentListPageBuilder arguments holder class
class StudentListPageBuilderArguments {
  final Key key;
  final void Function() onStudentListPage;
  StudentListPageBuilderArguments({this.key, this.onStudentListPage});
}

//TempoHolderPageBuilder arguments holder class
class TempoHolderPageBuilderArguments {
  final Key key;
  final void Function() onTempoHolderpage;
  TempoHolderPageBuilderArguments({this.key, this.onTempoHolderpage});
}

//UpdateClassPageBuilder arguments holder class
class UpdateClassPageBuilderArguments {
  final Key key;
  final void Function() onViewClassPage;
  final Class classInfo;
  UpdateClassPageBuilderArguments(
      {this.key, this.onViewClassPage, this.classInfo});
}

//RecordsPageBuilder arguments holder class
class RecordsPageBuilderArguments {
  final Key key;
  final void Function() onRecordsPage;
  RecordsPageBuilderArguments({this.key, this.onRecordsPage});
}

//StudentStatisticsPageBuilder arguments holder class
class StudentStatisticsPageBuilderArguments {
  final Key key;
  final void Function() onStudentStatisticsPage;
  StudentStatisticsPageBuilderArguments(
      {this.key, this.onStudentStatisticsPage});
}

//StudentsPageBuilder arguments holder class
class StudentsPageBuilderArguments {
  final Key key;
  final void Function() onStudentsPage;
  StudentsPageBuilderArguments({this.key, this.onStudentsPage});
}
