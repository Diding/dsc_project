import 'package:auto_route/auto_route_annotations.dart';
import 'package:dsc_project/app/auth_widget.dart';
import 'package:dsc_project/app/home/account/account_page.dart';
import 'package:dsc_project/app/home/list_view/add_class_page.dart';
import 'package:dsc_project/app/home/list_view/attendance_check_page.dart';
import 'package:dsc_project/app/home/list_view/manual_checklist_page.dart';
import 'package:dsc_project/app/home/list_view/class_page.dart';
import 'package:dsc_project/app/home/list_view/records_page.dart';
import 'package:dsc_project/app/home/list_view/student_statistics_page.dart';
import 'package:dsc_project/app/home/list_view/students_page.dart';
import 'package:dsc_project/app/home/list_view/update_class_page.dart';
import 'package:dsc_project/app/sign_in/email_password/email_password_sign_in_page.dart';
import 'package:dsc_project/app/home/list_view/add_student_page.dart';
import 'package:dsc_project/app/home/list_view/tempo_holder.dart';
import 'package:dsc_project/app/home/list_view/student_list_page.dart';

// flutter packages pub run build_runner build
// generate route.gr.dart file

@autoRouter
class $Router {
  @initial
  AuthWidget authWidget;

  @MaterialRoute(fullscreenDialog: true)
  EmailPasswordSignInPageBuilder emailPasswordSignInPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  AttendanceCheckPageBuilder attendanceCheckPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  AddClassPageBuilder addClassPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  ManualChecklistPageBuilder manualChecklistPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  ClassPageBuilder classPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  AddStudentPageBuilder addStudentPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  AccountPageBuilder accountPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  StudentListPageBuilder studentListPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  TempoHolderPageBuilder tempoHolderPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  UpdateClassPageBuilder updateClassPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  RecordsPageBuilder recordsPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  StudentStatisticsPageBuilder studentStatisticsPageBuilder;

  @MaterialRoute(fullscreenDialog: true)
  StudentsPageBuilder studentsPageBuilder;
}
