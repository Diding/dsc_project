import 'package:flutter/material.dart';

@immutable
class CustomListTile extends StatelessWidget {
  const CustomListTile(
      {Key key, this.icon, this.text, this.arrowIcon, this.onTap, this.label})
      : super(key: key);
  final IconData icon;
  final String text;
  final IconData arrowIcon;
  final Function onTap;
  final String label;


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: InkWell(
        splashColor: Colors.grey[400],
        onTap: onTap,
        child: Container(
            // margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
            // color: Color(0xFF272D30),
            height: 55,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(icon, color: Colors.teal, size: 40.0),
                    Padding(
                      padding: const EdgeInsets.all(14.0),
                      child: Text(
                        text,
                        style: TextStyle(
                            letterSpacing: 1.2,
                            wordSpacing: 1.0,
                            color: Colors.grey[800],
                            fontFamily: 'Open Sans',
                            fontSize: 13.0,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    Text(label,
                        style: TextStyle(
                            color: Colors.teal[700],
                            fontFamily: 'Open Sans',
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600)),
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
