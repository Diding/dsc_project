import 'package:dsc_project/common_widgets/custom_raised_button.dart';
import 'package:flutter/material.dart';

class FormSubmitButton extends CustomRaisedButton {
  FormSubmitButton({
    Key key,
    String text,
    bool loading = false,
    VoidCallback onPressed,
    BorderRadius borderRadius,
    
  }) : super(
          key: key,
          child: Text(
            text,
            style: TextStyle(color: Colors.white, fontSize: 20.0),
          ),
          height: 44.0,
          borderRadius: 5.0,
          color: Colors.black,
          textColor: Colors.black87,
          loading: loading,
          onPressed: onPressed,
    );
}
