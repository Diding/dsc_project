import 'package:dsc_project/app/home/home_page.dart';
import 'package:dsc_project/app/home/list_view/attendance_check_page.dart';
import 'package:flutter/material.dart';
import 'package:dsc_project/constants/strings.dart';

class PopUpMessage extends StatelessWidget {
  void popUpMessage(String title, String content, Function onPressed, BuildContext context) {
    var message = AlertDialog(
      title: Text(
        title,
        textAlign: TextAlign.center,
      ),
      content: Text(
        content,
        textAlign: TextAlign.center,
      ),
      actions: <Widget>[
        FlatButton(
          child: Text(Strings.ok),
          onPressed: onPressed
        ),
      ],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return message;
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
