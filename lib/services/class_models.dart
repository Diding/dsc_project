import 'package:cloud_firestore/cloud_firestore.dart';

class Class {
  String id;
  String className;
  String classSection;
  String startTime;
  String endTime;

  Class(
      {this.id,
      this.className,
      this.classSection,
      this.startTime,
      this.endTime});

  Class.fromMap(Map snapshot, String id)
      : id = id ?? '',
        className = snapshot['className'] ?? '',
        classSection = snapshot['classSection'] ?? '',
        startTime = snapshot['startTime'] ?? '',
        endTime = snapshot['endTime'] ?? '';

  toJson() {
    return {
      "className": className,
      "classSection": classSection,
      "startTime": startTime,
      "endTime": endTime
    };
  }
}

class Student {
  String studentId;
  String firstname;
  String lastname;
  String parentContactNumber;
  String classId;

  Student(
      {this.studentId,
      this.firstname,
      this.lastname,
      this.parentContactNumber,
      this.classId});

  Student.fromMap(Map snapshot, String studentId)
      : studentId = studentId ?? '',
        firstname = snapshot['firstname'] ?? '',
        lastname = snapshot['lastname'] ?? '',
        parentContactNumber = snapshot['parentContactNumber'] ?? '',
        classId = snapshot['classId'] ?? '';

  Student.fromJson(Map<String, dynamic> json)
      : firstname = json['firstname'],
        lastname = json['lastname'],
        parentContactNumber = json['parentContactNumber'];

  toJson() {
    return {
      "firstname": firstname,
      "lastname": lastname,
      "parentContactNumber": parentContactNumber
    };
  }
}

class ClassAttendance {
  String attendanceId;
  String classId;
  Timestamp dateChecked;
  String record;

  ClassAttendance({this.attendanceId, classId, this.dateChecked, this.record});

  ClassAttendance.fromMap(Map snapshot, String attendanceId)
      : attendanceId = attendanceId ?? '',
        classId = snapshot['classId'] ?? '',
        dateChecked = snapshot['dateChecked'],
        record = snapshot['record'] ?? '';
}

class Attendance {
  String student;
  String status;

  Attendance({this.student, this.status});

  Attendance.fromJson(Map<String, dynamic> json)
      : student = json['student'],
        status = json['status'];

  Map<String, String> toJson() => {"student": student, "status": status};
}

class StudentRecord {
  String studentId;
  int presentRecord;
  int absentRecord;
  int lateRecord;

  StudentRecord(
      {this.studentId, this.presentRecord, this.absentRecord, this.lateRecord});

  StudentRecord.fromJson(Map<dynamic, dynamic> json)
      : studentId = json['studentId'],
        presentRecord = json['presentRecord'],
        absentRecord = json['absentRecord'],
        lateRecord = json['lateRecord'];
}