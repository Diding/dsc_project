import 'package:cloud_firestore/cloud_firestore.dart';

class Api {
  final Firestore _db = Firestore.instance;
  final String path;
  var ref;

  Api({this.path}) {
    ref = _db.collection(path);
  }

  Future<QuerySnapshot> getDataCollection() {
    return ref.getDocuments();
  }

  Stream<QuerySnapshot> streamDataCollection(
      String path, String field, String id) {
    if (path == 'students') {
      return ref.where(field, isEqualTo: id).orderBy('lastname').snapshots();
    } else {
      return ref.where(field, isEqualTo: id).snapshots();
    }
  }

  Future<DocumentSnapshot> getDocumentById(String id) {
    return ref.document(id).get();
  }

  Future deleteDocumentById(String id) async {
    await ref.document(id).delete();
  }

  Future updateClassDocument(String className, String classSection,
      String startTime, String endTime, String id) async {
    return await ref.document(id).updateData({
      "className": className,
      "classSection": classSection,
      "startTime": startTime,
      "endTime": endTime
    });
  }

  Future updateStudentRecordDocument(String status, int statusRecord, String studentRecordId) async {
    return await ref.document(studentRecordId).updateData({
      "$status": statusRecord
    });
  }
}