import 'package:cloud_firestore/cloud_firestore.dart';

class ClassServices {
  final CollectionReference classCollection =
      Firestore.instance.collection('classes');

  Future<void> createClass(String classSection, String subjectName,
      String startTime, String endTime, String uid) async {
    await classCollection.add({
      'classSection': classSection,
      'className': subjectName,
      'endTime': endTime,
      'startTime': startTime,
      'uid': uid
    });
  }
}