import 'package:cloud_firestore/cloud_firestore.dart';

class StudentRecordServices {
  final CollectionReference studentRecordsCollection =
      Firestore.instance.collection('student_records');

  Future<void> submitStudentRecord(
      String studentId, int presentRecord, int absentRecord, int lateRecord) async {
    await studentRecordsCollection.add({
      'studentId': studentId,
      'presentRecord': presentRecord,
      'absentRecord': absentRecord,
      'lateRecord': lateRecord
    });
  }
}