import 'dart:async';
import 'package:meta/meta.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

String documentIdFromCurrentDate() => DateTime.now().toIso8601String();

class FirestoreDatabase {
  FirestoreDatabase({@required this.uid}) : assert(uid != null);
  final String uid;
  CollectionReference usersCollection = 
    Firestore.instance.collection('users');

  Future<void> addUser(String email, String displayName, String photoUrl) async {
    await usersCollection
      .document(uid)
      .setData({
        'email' : email, 
        'displayName' : displayName, 
        'photoUrl' : photoUrl
        }
      );
  }
}