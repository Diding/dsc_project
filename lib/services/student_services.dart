import 'package:cloud_firestore/cloud_firestore.dart';

class StudentServices {
  final CollectionReference studentCollection =
      Firestore.instance.collection('students');

  Future<String> addNewStudent(String classId, String firstname,
      String lastname, String parentContactNumber) async {
    DocumentReference ref = await studentCollection.add({
      'classId': classId,
      'firstname': firstname,
      'lastname': lastname,
      'parentContactNumber': parentContactNumber
    });
    return ref.documentID;
  }
}