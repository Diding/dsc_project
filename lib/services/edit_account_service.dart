import 'dart:async';
import 'package:meta/meta.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

String documentIdFromCurrentDate() => DateTime.now().toIso8601String();

class AccountDatabase {
  AccountDatabase({@required this.uid}) : assert(uid != null);
  final String uid;
  CollectionReference usersCollection = Firestore.instance.collection('users');

  Future editUser(String displayName, String photoUrl) async {
    return await usersCollection
      .document(uid)
      .updateData({
        'displayName' : displayName,
        'photoUrl' : photoUrl
      });
  }
}