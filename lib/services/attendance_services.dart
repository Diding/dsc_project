import 'package:cloud_firestore/cloud_firestore.dart';

class AttendanceServices {
  final CollectionReference attendanceCollection =
      Firestore.instance.collection('attendance');

  Future<void> submitAttendance(
      String classId, Timestamp dateChecked, String record) async {
    await attendanceCollection.add({
      'classId': classId,
      'dateChecked': dateChecked,
      'record': record
    });
  }
}