import 'dart:async';
import 'package:sms/sms.dart';

class FlutterSmsService {
  Future<void> sendMessage(String address, String body) async {
    
    SmsSender sender = SmsSender();
    SmsMessage message = SmsMessage(address, body);

    message.onStateChanged.listen((state) {
      if (state == SmsMessageState.Sent) {
        print("SENT!");
      } else if (state == SmsMessageState.Delivered) {
        print("DELIVERED!");
      } else {
        print("ERROR!");
      }
    });
    await sender.sendSms(message);
  }
}